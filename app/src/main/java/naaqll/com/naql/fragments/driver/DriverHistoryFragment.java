package naaqll.com.naql.fragments.driver;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.driver.ViewTripForDriverActivity;
import naaqll.com.naql.activities.owner.ViewTripForOwnerActivity;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.adapter.TripForOwnerAdapter;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.DriverOrdersController;
import naaqll.com.naql.model.Trip;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriverHistoryFragment extends BaseFragment implements BaseController.IResult<List<Trip>>, IAdapter<Trip> {

    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.btn_add)
    Button btnAdd;


    private static DriverHistoryFragment fragment;
    private TripForOwnerAdapter adapter;
    private DriverOrdersController controller;

    public static DriverHistoryFragment newInstance() {
        if (fragment == null)
            fragment = new DriverHistoryFragment();
        return fragment;
    }

    public DriverHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_owner_orders, container, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100){
            recycler.setVisibility(View.GONE);
            tvError.setVisibility(View.GONE);
            swipe.setRefreshing(true);
            controller.getTripsHistory();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        adapter = new TripForOwnerAdapter(mContext, this);
        recycler.setAdapter(adapter);
        controller = new DriverOrdersController(mContext, this, this);
        controller.getTripsHistory();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                controller.getTripsHistory();
            }
        });
        btnAdd.setVisibility(View.GONE);

    }

    @Override
    public void startLoading() {
        swipe.setRefreshing(true);
        tvError.setVisibility(View.INVISIBLE);
    }

    @Override
    public void endLoading(String error) {
        swipe.setRefreshing(false);
        controller.showErrorMessage(error);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(error);
    }

    @Override
    public void endLoading() {
        recycler.setVisibility(View.VISIBLE);
        swipe.setRefreshing(false);
    }

    @Override
    public void result(List<Trip> trips) {
        if (trips.size() == 0) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.no_trips_found));
        } else {
            adapter.setList(trips);
        }
    }

    @Override
    public void click(int position, Trip trip) {
        Intent intent = new Intent(mContext, ViewTripForDriverActivity.class);
        intent.putExtra("data", trip);
        startActivityForResult(intent,100);
    }
}
