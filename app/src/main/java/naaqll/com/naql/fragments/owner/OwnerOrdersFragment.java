package naaqll.com.naql.fragments.owner;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.owner.CreateOrderActivity;
import naaqll.com.naql.activities.owner.ViewTripForOwnerActivity;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.adapter.TripForOwnerAdapter;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.OwnerOrdersController;
import naaqll.com.naql.model.Trip;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnerOrdersFragment extends BaseFragment implements BaseController.IResult<List<Trip>>, IAdapter<Trip> {

    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.tv_error)
    TextView tvError;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    private static OwnerOrdersFragment fragment;
    private OwnerOrdersController controller;
    private TripForOwnerAdapter adapter;
    private int lastItemSelected = -1;

    public static OwnerOrdersFragment newInstance() {
        if (fragment == null)
            fragment = new OwnerOrdersFragment();
        return fragment;
    }

    public OwnerOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new OwnerOrdersController(mContext, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_owner_orders, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        adapter = new TripForOwnerAdapter(mContext, this);
        recycler.setAdapter(adapter);
        controller.getCurrentTripList(OwnerOrdersFragment.this);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                controller.getCurrentTripList(OwnerOrdersFragment.this);
            }
        });
    }

    @OnClick(R.id.btn_add)
    public void btnAdd() {
        startActivityForResult(new Intent(mContext, CreateOrderActivity.class), 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                /**Trip trip = (Trip) data.getSerializableExtra("data");
                adapter.insertItem(trip);*/
                controller.getCurrentTripList(OwnerOrdersFragment.this);
            } else if (requestCode == 110 && lastItemSelected != -1) {
                adapter.updateStatusToCancel(lastItemSelected);
            }
        }
    }

    @Override
    public void startLoading() {
        swipe.setRefreshing(true);
        tvError.setVisibility(View.INVISIBLE);
    }

    @Override
    public void endLoading(String error) {
        swipe.setRefreshing(false);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(error);
    }

    @Override
    public void endLoading() {
        swipe.setRefreshing(false);
    }

    @Override
    public void result(List<Trip> trips) {
        if (trips.size() == 0) {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText(getString(R.string.no_trip_create_one));
            recycler.setVisibility(View.GONE);
        } else {
            recycler.setVisibility(View.VISIBLE);
            tvError.setVisibility(View.GONE);
            adapter.setList(trips);
        }
    }

    @Override
    public void click(int position, Trip trip) {
        lastItemSelected = position;
        Intent intent = new Intent(mContext, ViewTripForOwnerActivity.class);
        intent.putExtra("data", trip);
        tvError.setVisibility(View.GONE);
        startActivityForResult(intent, 110);
    }
}
