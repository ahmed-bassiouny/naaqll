package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.AboutUsController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.WebPageType;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends BaseFragment implements BaseController.IResult<String> {


    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;
    @BindView(R.id.tv_error)
    TextView tvError;
    private AboutUsController controller;

    public static AboutUsFragment fragment;

    public static AboutUsFragment newInstanceAbout() {
        if (fragment == null)
            fragment = new AboutUsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.TYPE, WebPageType.ABOUT);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static AboutUsFragment newInstanceTerms() {
        if (fragment == null)
            fragment = new AboutUsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.TYPE, WebPageType.TERMS_CONDITIONS);
        fragment.setArguments(bundle);
        return fragment;
    }


    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        controller = new AboutUsController(mContext, this, this);
        if (getArguments() != null)
            controller.getData((WebPageType) getArguments().getSerializable(Constants.TYPE));
    }

    @Override
    public void result(String s) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL(null, s, "text/html", "utf-8", null);
    }

    @Override
    public void startLoading() {
        pbProgress.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);
    }

    @Override
    public void endLoading(String error) {
        pbProgress.setVisibility(View.GONE);
        tvError.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
        tvError.setText(error);
    }

    @Override
    public void endLoading() {
        pbProgress.setVisibility(View.GONE);
        tvError.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
    }
}
