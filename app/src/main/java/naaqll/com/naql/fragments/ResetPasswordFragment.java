package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.ResetPasswordController;
import naaqll.com.naql.helper.Validation;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordFragment extends BaseFragment {


    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    private ResetPasswordController controller;

    public ResetPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reset_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        controller = new ResetPasswordController(mContext,this);

    }

    @OnClick(R.id.btn_send)
    public void updatePassword() {
        if(etCode.getText().toString().isEmpty()){
            etCode.setError(getString(R.string.required));
        } else if (!Validation.validPassword(etPassword.getText().toString())) {
            etPassword.setError(getString(R.string.invalid_password));
        } else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            etConfirmPassword.setError(getString(R.string.invalid_confirm));
        } else {
            controller.resetPassword(etPassword.getText().toString(),etCode.getText().toString());
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }
}
