package naaqll.com.naql.fragments.driver;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Switch;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.CarController;
import naaqll.com.naql.helper.Utils;
import naaqll.com.naql.model.Car;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCarFragment extends BaseFragment implements BaseController.IResult<Car> {

    @BindView(R.id.et_car_type)
    TextInputEditText etCarType;
    @BindView(R.id.et_model)
    TextInputEditText etModel;
    @BindView(R.id.et_plate_number)
    TextInputEditText etPlateNumber;
    @BindView(R.id.et_color)
    TextInputEditText etColor;
    @BindView(R.id.btn_delete)
    Button btnDeleted;

    private Car car;
    private CarController controller;

    public AddCarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_car, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            car = (Car) getArguments().getSerializable("data");
            setDataUI();
        }else {
            car = new Car();
            btnDeleted.setVisibility(View.GONE);
        }

        controller = new CarController(mContext,this,this);
    }

    private void setDataUI() {
        etCarType.setText(car.getCarType());
        etModel.setText(car.getModel());
        etPlateNumber.setText(car.getPlateNumber());
        etColor.setText(car.getColor());
//        etFormNumber.setText(car.getFormNumber());
//        etFormExpirationDate.setText(car.getFormExpirationDate());
    }

    @OnClick(R.id.et_save)
    void onEtSaveClick() {
        if (validData()) {
            createOrUpdateCar();
        }
    }


    @OnClick(R.id.btn_delete)
    void onBtnDelete(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(getString(R.string.are_you_sure));
        builder.setCancelable(true);

        builder.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        car.setCarType(etCarType.getText().toString());
                        car.setModel(etModel.getText().toString());
                        car.setPlateNumber(etPlateNumber.getText().toString());
                        car.setColor(etColor.getText().toString());
                        car.setDeleted(true);
                        car.setCarId();
                        controller.editCar(car);
                    }
                });

        builder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void createOrUpdateCar() {
        car.setCarType(etCarType.getText().toString());
        car.setModel(etModel.getText().toString());
        car.setPlateNumber(etPlateNumber.getText().toString());
        car.setColor(etColor.getText().toString());
//        car.setFormNumber(etFormNumber.getText().toString());
//        car.setFormExpirationDate(etFormExpirationDate.getText().toString());
        if (car.getId()== 0) {
            // add
            controller.addCar(car);
        }else {
            // edit
            car.setCarId();
            controller.editCar(car);
        }
    }

    private boolean validData() {
        // check error
        if (Utils.validEditText(etCarType) &&
                Utils.validEditText(etModel) &&
                Utils.validEditText(etPlateNumber) &&
                Utils.validEditText(etColor) ){
                //Utils.validEditText(etFormNumber) && Utils.validEditText(etFormExpirationDate)) {
            return true;
        }
        return false;
    }


    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {
        getMyActivity().onBackPressed();
    }

    @Override
    public void result(Car car) {

    }
}
