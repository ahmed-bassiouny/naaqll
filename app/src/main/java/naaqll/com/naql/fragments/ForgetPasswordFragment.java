package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.AuthController;
import naaqll.com.naql.helper.Validation;
import retrofit2.http.Body;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPasswordFragment extends BaseFragment {

    private AuthController controller;
    @BindView(R.id.et_email)
    EditText etEmail;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_froget_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        controller = new AuthController(mContext, this);
    }

    @OnClick(R.id.btn_send)
    public void forgetPassword() {
        if (Validation.validEmail(etEmail.getText().toString()))
            controller.forgetPassword(etEmail.getText().toString());
        else
            etEmail.setError(getString(R.string.invalid_email));
    }

    @OnClick(R.id.tv_have_code)
    public void haveCode() {
        MyFragmentTransaction.open(getMyActivity(), new ResetPasswordFragment(),R.id.main_frame,"forget");
    }


    @OnClick(R.id.iv_back)
    public void back() {
        getMyActivity().onBackPressed();
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }
}
