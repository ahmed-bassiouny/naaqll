package naaqll.com.naql.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.AuthActivity;
import naaqll.com.naql.activities.ChangeLanguageActivity;
import naaqll.com.naql.activities.WebPageActivity;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.LoginController;
import naaqll.com.naql.helper.Validation;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment {


    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;

    private LoginController controller;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.login));
        ButterKnife.bind(this, view);
        controller = new LoginController(mContext,this);
    }



    @OnClick(R.id.tv_change_language)
    public void changeLanguage() {
        startActivity(new Intent(mContext,ChangeLanguageActivity.class));
    }


    @OnClick(R.id.tv_condition)
    public void terms() {
        startActivity(new Intent(mContext,WebPageActivity.class));
    }

    @OnClick(R.id.tv_create_account)
    public void register() {
        MyFragmentTransaction.open(getMyActivity(), new ChooseUserTypeFragment(), R.id.main_frame, "login");
    }

    @OnClick(R.id.tv_forget_password)
    public void forgetPassword() {
        MyFragmentTransaction.open(getMyActivity(), new ForgetPasswordFragment(), R.id.main_frame, "login");
    }

    @OnClick(R.id.btn_login)
    public void login() {
        if (!Validation.validEmail(etEmail.getText().toString())) {
            etEmail.setError(getString(R.string.invalid_email));
        } else if (!Validation.validPassword(etPassword.getText().toString())) {
            etPassword.setError(getString(R.string.invalid_password));
        } else {
            controller.login(etEmail.getText().toString(),etPassword.getText().toString());
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }
}
