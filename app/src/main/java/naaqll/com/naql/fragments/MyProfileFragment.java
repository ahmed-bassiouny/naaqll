package naaqll.com.naql.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.ChangeLanguageActivity;
import naaqll.com.naql.activities.EditProfileActivity;
import naaqll.com.naql.activities.SplashActivity;
import naaqll.com.naql.activities.driver.CarActivity;
import naaqll.com.naql.activities.driver.NotificationSettingActivity;
import naaqll.com.naql.activities.owner.PackageActivity;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.MyProfileController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment implements BaseController.IResult<String> {

    @BindView(R.id.company_name)
    TextView companyName;
    @BindView(R.id.tv_commercial_registration_no)
    TextView tvCommercialRegistrationNo;
    @BindView(R.id.linear_company_activity)
    LinearLayout linearCompanyActivity;
    @BindView(R.id.tv_company_activity)
    TextView tvCompanyActivity;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_point)
    TextView tvPoint;
  /*  @BindView(R.id.btn_drivers)
    Button btnDrivers;*/
    /*@BindView(R.id.btn_package)
    Button btnPackage;*/
  /*  @BindView(R.id.btn_notification)
    Button btnNotification;*/
    /*@BindView(R.id.btn_cars)
    Button btnCars;*/

    private static MyProfileFragment fragment;
    private MyProfileController controller;

    public static MyProfileFragment newInstance(){
        if(fragment == null)
            fragment = new MyProfileFragment();
        return fragment;
    }

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_profile, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        controller = new MyProfileController(mContext,this);
    }

    @Override
    public void onStart() {
        super.onStart();
        setDataUI();
    }

    private void setDataUI() {
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        companyName.setText(user.getCompanyName());
        tvCommercialRegistrationNo.setText(user.getCommercialNumber());
        tvEmail.setText(user.getEmail());
        tvPhone.setText(user.getPhone());
        if (user.getUserType() == UserType.DRIVER_COMPANIES) {
            linearCompanyActivity.setVisibility(View.GONE);
            //btnDrivers.setVisibility(View.VISIBLE);
            //btnPackage.setVisibility(View.GONE);
            tvPoint.setVisibility(View.GONE);
            //btnNotification.setVisibility(View.VISIBLE);
            //btnCars.setVisibility(View.VISIBLE);
        } else {
            linearCompanyActivity.setVisibility(View.VISIBLE);
            tvCompanyActivity.setText(user.getCompanyActivity());
            //btnDrivers.setVisibility(View.GONE);
            //btnPackage.setVisibility(View.VISIBLE);
            tvPoint.setVisibility(View.VISIBLE);
            //btnNotification.setVisibility(View.GONE);
            //btnCars.setVisibility(View.GONE);
            tvPoint.setText(String.format("%s %s",getString(R.string.your_point),user.getPoints()));
            controller.getPoint(this);
        }
    }

    @OnClick(R.id.et_edit_profile)
    void editProfileClick() {
        startActivity(new Intent(mContext, EditProfileActivity.class));
    }


  /*  @OnClick(R.id.btn_change_language)
    void changeLanguageClick() {
        startActivity(new Intent(mContext, ChangeLanguageActivity.class));
    }*/

/*
    @OnClick(R.id.btn_drivers)
    void showDriversClick() {
        startActivity(new Intent(mContext, DriverListActivity.class));
    }*/


   /* @OnClick(R.id.btn_package)
    void showPackagesClick() {
        startActivity(new Intent(mContext, PackageActivity.class));
    }

*/
   /* @OnClick(R.id.btn_sign_out)
    void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(getString(R.string.are_you_sure));
        builder.setCancelable(true);

        builder.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPrefManager.clearSharedPref();
                        dialog.cancel();
                        getMyActivity().finish();
                        startActivity(new Intent(mContext, SplashActivity.class));
                    }
                });

        builder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }*/

    /*@OnClick(R.id.btn_notification)
    void showNotificationsClick() {
        startActivity(new Intent(mContext, NotificationSettingActivity.class));
    }*/

    /*@OnClick(R.id.btn_cars)
    void showCarsClick() {
        startActivity(new Intent(mContext, CarActivity.class));
    }
*/
    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }

    @Override
    public void result(String s) {
        if(getActivity()!= null && isAdded()) {
            User user = SharedPrefManager.getObject(Constants.USER, User.class);
            user.setPoints(s);
            tvPoint.setText(String.format("%s %s", getString(R.string.your_point), s));
            SharedPrefManager.setObject(Constants.USER, user);
        }
    }
}
