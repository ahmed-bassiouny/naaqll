package naaqll.com.naql.fragments.driver;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.CarAdapter;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.CarController;
import naaqll.com.naql.model.Car;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewCarsFragment extends BaseFragment implements BaseController.IResult<List<Car>>, IAdapter<Car> {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    private CarAdapter carAdapter;
    private CarController controller;

    public ViewCarsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_cars, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        carAdapter = new CarAdapter(mContext, this);
        recycler.setAdapter(carAdapter);
        controller = new CarController(mContext,this,this);
        controller.getCars();
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                controller.getCars();
            }
        });
    }

    @OnClick(R.id.add)
    void addDriver() {
        MyFragmentTransaction.open(getMyActivity(),new AddCarFragment(),R.id.main_frame,"back");
    }

    @Override
    public void startLoading() {
        swipe.setRefreshing(true);
    }

    @Override
    public void endLoading(String error) {
        swipe.setRefreshing(false);
    }

    @Override
    public void endLoading() {
        swipe.setRefreshing(false);
    }

    @Override
    public void result(List<Car> cars) {
        carAdapter.addList(cars);
        recycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void click(int position, Car car) {
        AddCarFragment fragment = new AddCarFragment();
        Bundle b = new Bundle();
        b.putSerializable("data",car);
        fragment.setArguments(b);
        MyFragmentTransaction.open(getMyActivity(),fragment,R.id.main_frame,"back");
    }
}
