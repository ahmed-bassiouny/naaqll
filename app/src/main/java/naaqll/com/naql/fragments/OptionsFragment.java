package naaqll.com.naql.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.ChangeLanguageActivity;
import naaqll.com.naql.activities.SplashActivity;
import naaqll.com.naql.activities.TripSearchActivity;
import naaqll.com.naql.activities.WebPageActivity;
import naaqll.com.naql.activities.driver.CarActivity;
import naaqll.com.naql.activities.driver.DriverListActivity;
import naaqll.com.naql.activities.driver.NotificationSettingActivity;
import naaqll.com.naql.activities.owner.PackageActivity;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.adapter.OptionAdapter;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.SimpleDividerItemDecoration;
import naaqll.com.naql.model.Option;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionsFragment extends Fragment implements IAdapter<Integer> {


    List<Option> options = new ArrayList<>();

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private static OptionsFragment fragment;

    public static OptionsFragment newInstance(){
        if(fragment == null)
            fragment = new OptionsFragment();
        return fragment;
    }

    public OptionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_options, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        if(getContext() != null)
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        options.clear();
        options.add(new Option(1,getString(R.string.about_us),getResources().getDrawable(R.drawable.about_us)));
        options.add(new Option(7,getString(R.string.search_for_bill),getResources().getDrawable(R.drawable.ic_search_24dp)));
        User user = SharedPrefManager.getObject(Constants.USER, User.class);

        if (user.getUserType() == UserType.DRIVER_COMPANIES) {

            options.add(new Option(2,getString(R.string.notification),getResources().getDrawable(R.drawable.notify_icon)));
            options.add(new Option(3,getString(R.string.cars),getResources().getDrawable(R.drawable.car_icon)));
            options.add(new Option(8,getString(R.string.drivers),getResources().getDrawable(R.drawable.delivery)));
        } else {
            options.add(new Option(4,getString(R.string.view_package),getResources().getDrawable(R.drawable.package_icon)));
        }

        options.add(new Option(5,getString(R.string.change_language),getResources().getDrawable(R.drawable.language_icon)));
        options.add(new Option(6,getString(R.string.logout),getResources().getDrawable(R.drawable.remove_icon)));

        recyclerView.setAdapter(new OptionAdapter(options,this));
    }

    @Override
    public void click(int position, Integer integer) {
        if(getContext() == null)
            return;
        switch (integer){
            case 1:
                Intent intent = new Intent(getContext(), WebPageActivity.class);
                intent.putExtra("about",true);
                startActivity(intent);
                break;
            case 2:
                startActivity(new Intent(getContext(), NotificationSettingActivity.class));
                break;
            case 3:
                startActivity(new Intent(getContext(), CarActivity.class));
                break;
            case 4:
                startActivity(new Intent(getContext(), PackageActivity.class));
                break;
            case 5:
                startActivity(new Intent(getContext(), ChangeLanguageActivity.class));
                break;
            case 6:
                logout();
                break;
            case 7:
                startActivity(new Intent(getContext(), TripSearchActivity.class));
                break;
            case 8:
                startActivity(new Intent(getContext(), DriverListActivity.class));
                break;
        }
    }

    void logout() {
        if(getActivity() == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.are_you_sure));
        builder.setCancelable(true);

        builder.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPrefManager.clearSharedPref();
                        dialog.cancel();
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), SplashActivity.class));
                    }
                });

        builder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }


}
