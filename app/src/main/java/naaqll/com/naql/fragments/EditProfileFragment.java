package naaqll.com.naql.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.MapActivity;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.EditAccountController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Validation;
import naaqll.com.naql.model.CitiesAndRegions;
import naaqll.com.naql.model.MyPlace;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends BaseFragment {


    @BindView(R.id.et_company_name)
    EditText etCompanyName;
    @BindView(R.id.et_company_activity)
    EditText etCompanyActivity;
    @BindView(R.id.tv_company_activity)
    TextInputLayout tvCompanyActivity;
    @BindView(R.id.et_commercial_number)
    EditText etCommercialNumber;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.sp_city)
    Spinner spCity;

    private EditAccountController controller;
    private User user;

    public EditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        controller = new EditAccountController(mContext, this);
        setDataUI();
    }

    private void setDataUI() {
        user = SharedPrefManager.getObject(Constants.USER, User.class);
        etCompanyName.setText(user.getCompanyName());
        etCommercialNumber.setText(user.getCommercialNumber());
        etCompanyActivity.setText(user.getCompanyActivity());
        etPhone.setText(user.getPhone());
        if (user.getUserType() == UserType.DRIVER_COMPANIES) {
            tvCompanyActivity.setVisibility(View.GONE);
        } else {
            tvCompanyActivity.setVisibility(View.VISIBLE);
        }
        List<String> cities = Arrays.asList(getResources().getStringArray(R.array.ksa_cities));
        int index = cities.indexOf(user.getCity());
        spCity.setSelection(index);
    }

    @OnClick(R.id.btn_save)
    public void updateProfile() {
        if (etCompanyName.getText().toString().trim().isEmpty()) {
            etCompanyName.setError(getString(R.string.invalid_company_name));
        } else if (etCommercialNumber.getText().toString().trim().isEmpty()) {
            etCommercialNumber.setError(getString(R.string.invalid_commercial_number));
        } else {
            if (user.getUserType() == UserType.DRIVER_COMPANIES) {
                controller.updateDriverProfile(user, etCompanyName.getText().toString(),
                        etCommercialNumber.getText().toString(),
                        etPhone.getText().toString(),getString(R.string.ksa),spCity.getSelectedItem().toString());
            } else {
                controller.updateOwnerProfile(user, etCompanyName.getText().toString(),
                        etCompanyActivity.getText().toString(),
                        etCommercialNumber.getText().toString(),
                        etPhone.getText().toString(),getString(R.string.ksa),spCity.getSelectedItem().toString());
            }
        }
    }

    @OnClick(R.id.btn_edit_password)
    public void updatePassword() {
        MyFragmentTransaction.open(getMyActivity(), new ChangePasswordFragment(),R.id.main_frame,"edit_profile");
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }
}
