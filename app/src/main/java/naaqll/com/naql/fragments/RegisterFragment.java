package naaqll.com.naql.fragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.WebPageActivity;
import naaqll.com.naql.activities.owner.CreateOrderActivity;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.RegisterController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Validation;
import naaqll.com.naql.model.CitiesAndRegions;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends BaseFragment {


    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.company_name)
    EditText etCompanyName;
    @BindView(R.id.company_activity)
    EditText etCompanyActivity;
    @BindView(R.id.commercial_registration_no)
    EditText etCommercialRegistrationNo;
    @BindView(R.id.email)
    EditText etEmail;
    @BindView(R.id.phone)
    EditText etPhone;
    @BindView(R.id.password)
    EditText etPassword;
    @BindView(R.id.confirm_password)
    EditText etConfirmPassword;

    @BindView(R.id.commercial_registration_date)
    EditText commercialRegistrationDate;

    @BindView(R.id.license_name)
    EditText licenseName;
    @BindView(R.id.license_number)
    EditText licenseNumber;
    @BindView(R.id.license_date)
    EditText licenseDate;

    @BindView(R.id.rb_ksa)
    RadioButton rbKsa;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_region)
    TextView tvRegion;
    @BindView(R.id.tax_number)
    EditText taxNumber;

    private int cityIndex = -1;
    private int regionIndex = -1;
    private RegisterController controller;
    private String[] arrs ;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
    }

    @OnClick(R.id.tv_city)
    public void cityClick(){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        if(rbKsa.isChecked()){
            arrs = CitiesAndRegions.getCitiesKSA();
        }else {
            arrs = CitiesAndRegions.getCitiesAUE();
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvCity.setText(arrs[which]);
                cityIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.tv_region)
    public void regionClick(){
        if(cityIndex == -1){
            controller.showErrorMessage(getString(R.string.please_choose_region));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        if(rbKsa.isChecked()){
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.ksaIndex,cityIndex);
        }else {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.aueIndex,cityIndex);
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvRegion.setText(arrs[which]);
                regionIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void init() {
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        if (user.getUserType() == UserType.DRIVER_COMPANIES) {
            etCompanyActivity.setVisibility(View.GONE);
            licenseName.setVisibility(View.VISIBLE);
            licenseNumber.setVisibility(View.VISIBLE);
            licenseDate.setVisibility(View.VISIBLE);
            tvRegister.setText(R.string.register_as_driver);
        }else {
            etCompanyActivity.setVisibility(View.VISIBLE);
            licenseName.setVisibility(View.GONE);
            licenseNumber.setVisibility(View.GONE);
            licenseDate.setVisibility(View.GONE);
            tvRegister.setText(R.string.register_as_company);
        }
        controller = new RegisterController(mContext, this);
    }

    @OnClick(R.id.iv_back)
    public void back() {
        getMyActivity().onBackPressed();
    }

    @OnClick(R.id.tv_condition)
    public void terms() {
        startActivity(new Intent(mContext, WebPageActivity.class));
    }

    @OnClick(R.id.btn_register)
    public void register() {
        if (etCompanyName.getText().toString().trim().isEmpty()) {
            etCompanyName.setError(getString(R.string.invalid_company_name));
        } else if (etCommercialRegistrationNo.getText().toString().length() != 10) {
            etCommercialRegistrationNo.setError(getString(R.string.invalid_commercial_number));
        } else if (commercialRegistrationDate.getText().toString().trim().isEmpty()) {
            commercialRegistrationDate.setError(getString(R.string.required));
        } else if (!Validation.validEmail(etEmail.getText().toString())) {
            etEmail.setError(getString(R.string.invalid_email));
        }

        // user is drivers
        else if (licenseName.getVisibility() == View.VISIBLE && licenseName.getText().toString().trim().isEmpty()) {
            licenseName.setError(getString(R.string.required));
        } else if (licenseNumber.getVisibility() == View.VISIBLE && licenseNumber.getText().toString().trim().isEmpty()) {
            licenseNumber.setError(getString(R.string.required));
        } else if (licenseDate.getVisibility() == View.VISIBLE && licenseDate.getText().toString().trim().isEmpty()) {
            licenseDate.setError(getString(R.string.required));
        }

        else if (!Validation.validPassword(etPassword.getText().toString())) {
            etPassword.setError(getString(R.string.invalid_password));
        } else if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            etConfirmPassword.setError(getString(R.string.invalid_confirm));
        }
        else if (cityIndex == -1 || regionIndex == -1){
            controller.showErrorMessage(getString(R.string.required_fileds));
        }
        else if(taxNumber.getText().toString().trim().isEmpty()){
            controller.showErrorMessage(getString(R.string.required_fileds));
        }

        else {
            int indexCountry = rbKsa.isChecked() ? 0 : 1;
            controller.register(etCompanyName.getText().toString()
                    , etCompanyActivity.getText().toString()
                    , etCommercialRegistrationNo.getText().toString()
                    , etEmail.getText().toString()
                    , etPhone.getText().toString()
                    , etPassword.getText().toString(), getString(R.string.ksa), String.valueOf(CitiesAndRegions.getRegionId(indexCountry,cityIndex,regionIndex))
                    ,commercialRegistrationDate.getText().toString(),licenseName.getText().toString()
                    ,licenseNumber.getText().toString(),licenseDate.getText().toString(),taxNumber.getText().toString());
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }


    @OnClick(R.id.commercial_registration_date)
    public void selectCommercialDate() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        DatePickerDialog dialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        commercialRegistrationDate.setText(String.format(Locale.getDefault(), "%d-%d-%d", year, (1 + monthOfYear), dayOfMonth));

                    }
                }, mYear, mMonth, mDay);
        dialog.getDatePicker().setMinDate(c.getTime().getTime());
        dialog.show();
    }

    @OnClick(R.id.license_date)
    public void selectLicenseDate() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        DatePickerDialog dialog =  new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        licenseDate.setText(String.format(Locale.getDefault(), "%d-%d-%d", year, (1 + monthOfYear), dayOfMonth));

                    }
                }, mYear, mMonth, mDay);
        dialog.getDatePicker().setMinDate(c.getTime().getTime());
        dialog.show();
    }
}
