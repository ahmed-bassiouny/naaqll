package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.EditAccountController;
import naaqll.com.naql.helper.Validation;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment {


    @BindView(R.id.et_old_password)
    TextInputEditText etOldPassword;
    @BindView(R.id.et_new_password)
    TextInputEditText etNewPassword;
    @BindView(R.id.et_confirm_password)
    TextInputEditText etConfirmPassword;
    private EditAccountController controller;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        controller = new EditAccountController(mContext,this);
    }


    @OnClick(R.id.btn_save)
    void saveClick() {
        if(!Validation.validPassword(etNewPassword.getText().toString())){
            etNewPassword.setError(getString(R.string.invalid_password));
        }else if(!etNewPassword.getText().toString().equals(etConfirmPassword.getText().toString())){
            etConfirmPassword.setError(getString(R.string.invalid_confirm));
        }else {
            controller.updatePassword(etOldPassword.getText().toString(),etNewPassword.getText().toString());
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }
}
