package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.AddCompliantActivity;
import naaqll.com.naql.adapter.CompliantAdapter;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.ComplaintsController;
import naaqll.com.naql.helper.SimpleDividerItemDecoration;
import naaqll.com.naql.model.Compliant;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintsFragment extends BaseFragment implements BaseController.IResult<List<Compliant>> {


    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.tv_no_compliant)
    TextView tvNoCompliant;
    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;
    private ComplaintsController controller;
    private CompliantAdapter adapter;
    public static ComplaintsFragment fragment;
    private List<Compliant> compliants;


    public static ComplaintsFragment newInstance() {
        if (fragment == null)
            fragment = new ComplaintsFragment();
        return fragment;
    }

    public ComplaintsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_complaints, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        recycler.setLayoutManager(new LinearLayoutManager(mContext));
        recycler.addItemDecoration(new SimpleDividerItemDecoration(mContext));
        controller = new ComplaintsController(mContext, this);
        adapter = new CompliantAdapter();
        recycler.setAdapter(adapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            recycler.setVisibility(View.GONE);
            tvNoCompliant.setVisibility(View.GONE);
            pbProgress.setVisibility(View.VISIBLE);
            controller.getComplaints(this);
        }
    }

    @OnClick(R.id.btn_new)
    public void newCompliant() {
        controller.launchActivity(AddCompliantActivity.class);
    }

    @Override
    public void result(List<Compliant> compliants) {
        this.compliants = compliants;
        if (compliants.size() == 0) {
            tvNoCompliant.setVisibility(View.VISIBLE);
            return;
        }
        adapter.addList(compliants);
        recycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void startLoading() {
        pbProgress.setVisibility(View.VISIBLE);
        recycler.setVisibility(View.GONE);
        tvNoCompliant.setVisibility(View.GONE);
    }

    @Override
    public void endLoading(String error) {
        pbProgress.setVisibility(View.GONE);
        recycler.setVisibility(View.GONE);
        tvNoCompliant.setVisibility(View.VISIBLE);
        tvNoCompliant.setText(error);
    }

    @Override
    public void endLoading() {
        pbProgress.setVisibility(View.GONE);
        recycler.setVisibility(View.VISIBLE);
        tvNoCompliant.setVisibility(View.GONE);
    }
}
