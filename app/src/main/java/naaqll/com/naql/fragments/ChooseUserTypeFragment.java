package naaqll.com.naql.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseFragment;
import naaqll.com.naql.controller.AuthController;
import naaqll.com.naql.model.UserType;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseUserTypeFragment extends BaseFragment {


    private AuthController controller;

    public ChooseUserTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_user_type, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

    }


    @OnClick({R.id.FrameLayout_companies,R.id.img_companies,R.id.tv_companies})
    public void setUserTypeCompanies() {
        getController().setUserType(UserType.DRIVER_COMPANIES);
    }

    @OnClick({R.id.FrameLayout_factories,R.id.img__factories,R.id.tv__factories})
    public void setUserTypeFactories() {
        getController().setUserType(UserType.FACTORIES);
    }


    @OnClick(R.id.iv_back)
    public void back() {
        getMyActivity().onBackPressed();
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void endLoading(String error) {

    }

    @Override
    public void endLoading() {

    }

    private AuthController getController(){
        if(controller == null)
        controller = new AuthController(mContext, this);
        return controller;
    }
}
