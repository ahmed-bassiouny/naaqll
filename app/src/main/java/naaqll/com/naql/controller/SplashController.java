package naaqll.com.naql.controller;

import android.content.Context;

import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.activities.AuthActivity;
import naaqll.com.naql.activities.ChooseLanguageActivity;
import naaqll.com.naql.activities.driver.HomeDriverActivity;
import naaqll.com.naql.activities.owner.HomeOwnerActivity;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.GeneralInteractor;
import naaqll.com.naql.model.CitiesAndRegions;
import naaqll.com.naql.model.Country;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;


public class SplashController extends BaseController implements RequestCallback<List<Country>> {

    private GeneralInteractor interactor;

    public SplashController(Context context) {
        super(context);
        interactor = new GeneralInteractor();
    }

    private void openNextScreen() {
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        if (user == null || user.getLang().isEmpty()) {
            // open choose language
            launchActivityWithFinish(ChooseLanguageActivity.class);
        } else if (user.getId() == 0) {
            // open man screen
            launchActivityWithFinish(AuthActivity.class);
        }else if(user.getUserType() == UserType.FACTORIES) {
            launchActivityWithFinish(HomeOwnerActivity.class);
        }else if(user.getUserType() == UserType.DRIVER_COMPANIES) {
            launchActivityWithFinish(HomeDriverActivity.class);
        }
    }
    public void getCountries(){
        interactor.getCountries(this);
    }

    @Override
    public void success(List<Country> countries) {
        CitiesAndRegions.setCountries(countries);
        openNextScreen();
    }

    @Override
    public void failed(String msg) {
        showErrorMessage(msg);
        getMyActivity().finish();
    }
}
