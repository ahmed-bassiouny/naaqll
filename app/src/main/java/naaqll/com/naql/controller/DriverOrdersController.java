package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.TripInteractor;
import naaqll.com.naql.model.Trip;

public class DriverOrdersController extends BaseController implements RequestCallback<BaseList<Trip>> {

    private ITripInteractor interactor;
    private IResult<List<Trip>> result;

    public DriverOrdersController(Context context, Fragment fragment, IResult<List<Trip>> result) {
        super(context, fragment);
        interactor = new TripInteractor();
        this.result = result;
    }

    public void getCurrentTrips() {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        getFragment().startLoading();
        interactor.getCurrentTrips(0, 30, "1", this);
    }

    public void getTripsHistory() {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        getFragment().startLoading();
        interactor.getTripsHistory(0, 30, "1", this);
    }

    @Override
    public void success(BaseList<Trip> tripBaseList) {
        result.result(tripBaseList.getDataList());
        getFragment().endLoading();

    }

    @Override
    public void failed(String msg) {
        getFragment().endLoading(msg);
    }
}
