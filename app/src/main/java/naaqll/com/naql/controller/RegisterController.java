package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.activities.driver.HomeDriverActivity;
import naaqll.com.naql.activities.owner.HomeOwnerActivity;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.interactor.UserInteractor;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

public class RegisterController extends BaseController implements RequestCallback<User> {

    private IUserInteractor interactor;

    public RegisterController(Context context, Fragment fragment) {
        super(context, fragment);
        interactor = new UserInteractor();
    }

    public void register(String companyName, String companyActivity,
                         String companyCommercialNo, String email,
                         String phone, String password,String country,String city,
                         String commercialDate,String licenseName,String licenseNumber,
                         String licenseDate,String taxNumber) {
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        User user = SharedPrefManager.getObject(Constants.USER,User.class);
        user.setCompanyName(companyName);
        user.setCompanyActivity(companyActivity);
        user.setCommercialNumber(companyCommercialNo);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        user.setIsApproved(true);
        user.setCountry(country);
        user.setCity(city);
        user.setCommercialDate(commercialDate);
        user.setLicenseName(licenseName);
        user.setLicenseNumber(licenseNumber);
        user.setLicenseDate(licenseDate);
        user.setTaxNumber(taxNumber);
        interactor.register(user, this);
    }

    @Override
    public void success(User newUser) {
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        newUser.setLang(user.getLang());
        SharedPrefManager.setObject(Constants.USER, newUser);
        hideDialog();
        if (user.getUserType() == UserType.DRIVER_COMPANIES)
            launchActivityWithFinishAndClearStack(HomeDriverActivity.class);
        else
            launchActivityWithFinishAndClearStack(HomeOwnerActivity.class);
        getMyActivity().finish();
    }

    @Override
    public void failed(String msg) {
        hideDialog();
        showErrorMessage(msg);
    }
}
