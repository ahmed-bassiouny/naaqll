package naaqll.com.naql.controller;

import android.content.Context;
import android.widget.TableRow;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.TripInteractor;
import naaqll.com.naql.model.Trip;

public class CreateOrderController extends BaseController implements RequestCallback<Trip> {

    private ITripInteractor interactor;
    private IResult<Trip> result;
    public CreateOrderController(Context context) {
        super(context);
        interactor = new TripInteractor();
    }

    public void createTrip(Trip trip,IResult<Trip> result){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        this.result = result;
        interactor.createTrip(trip,this);
    }

    @Override
    public void success(Trip trip) {
        result.result(trip);
        hideDialog();
    }

    @Override
    public void failed(String msg) {
        showErrorMessage(msg);
        hideDialog();
    }
}
