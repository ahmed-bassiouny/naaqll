package naaqll.com.naql.controller;

import android.content.Context;

import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.DriverInteractor;
import naaqll.com.naql.interactor.IDriverInteractor;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.User;

public class DriversController extends BaseController {
    private IDriverInteractor interactor;
    private IResult<List<Driver>> result;
    private int ownerID;

    public DriversController(Context context) {
        super(context);
        interactor = new DriverInteractor();
        ownerID = SharedPrefManager.getObject(Constants.USER, User.class).getId();
    }

    public void getDriver(IResult<List<Driver>> result) {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        showDialog();
        this.result = result;
        interactor.getDriver(ownerID, callback);
    }

    private RequestCallback<List<Driver>> callback = new RequestCallback<List<Driver>>() {
        @Override
        public void success(List<Driver> drivers) {
            result.result(drivers);
            hideDialog();
        }

        @Override
        public void failed(String msg) {
            showMessage(msg);
            hideDialog();
        }
    };
}
