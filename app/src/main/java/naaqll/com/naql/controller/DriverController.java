package naaqll.com.naql.controller;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.DriverInteractor;
import naaqll.com.naql.interactor.IDriverInteractor;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.User;

import static android.app.Activity.RESULT_OK;

public class DriverController extends BaseController implements RequestCallback {
    private IDriverInteractor interactor;
    private IResult<Driver> result;
    private int ownerID;


    public DriverController(Context context, IResult<Driver> result) {
        super(context);
        interactor = new DriverInteractor();
        ownerID = SharedPrefManager.getObject(Constants.USER, User.class).getId();
        this.result = result;
    }

    // for add driver for every company not to trip
    public void addDriver(Driver driver) {
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        driver.setOwnerId(String.valueOf(ownerID));
        interactor.addDriver(driver,callback);
    }

    public void editDriver(Driver driver) {
        showDialog();
        interactor.editDriver(driver,callback);

    }

    public void deleteDriver(int driverId, final IResult result) {
        showDialog();
        interactor.deleteDriver(driverId, new RequestCallback() {
            @Override
            public void success(Object o) {
                hideDialog();
                result.result(null);

            }

            @Override
            public void failed(String msg) {
                hideDialog();
                showMessage(msg);
            }
        });

    }
    private RequestCallback<Driver> callback = new RequestCallback<Driver>() {
        @Override
        public void success(Driver driver) {
            hideDialog();
            result.result(driver);
        }

        @Override
        public void failed(String msg) {
            showMessage(msg);
            hideDialog();
        }
    };

    // for add driver for every trip
    public void addDriver(int tripId,List<Driver> drivers){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        interactor.addDriverToTrip(ownerID,tripId,drivers,this);
    }

    @Override
    public void success(Object o) {
        hideDialog();
        result.result(null);

    }

    @Override
    public void failed(String msg) {
        showErrorMessage(msg);
        hideDialog();
    }
}
