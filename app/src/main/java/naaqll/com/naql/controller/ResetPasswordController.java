package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import naaqll.com.naql.R;
import naaqll.com.naql.activities.AuthActivity;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.interactor.UserInteractor;

public class ResetPasswordController extends BaseController implements RequestCallback {

    private IUserInteractor interactor;

    public ResetPasswordController(Context context, Fragment fragment) {
        super(context, fragment);
        interactor = new UserInteractor();
    }

    public void resetPassword(String password,String code){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        interactor.resetPassword(password,code,this);
    }

    @Override
    public void success(Object o) {
        hideDialog();
        showMessage(getContext().getString(R.string.reset_password_successful));
        launchActivityWithFinishAndClearStack(AuthActivity.class);
    }

    @Override
    public void failed(String msg) {
        hideDialog();
        showMessage(msg);
    }
}
