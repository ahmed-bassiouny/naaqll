package naaqll.com.naql.controller;

import android.content.Context;

import naaqll.com.naql.R;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ComplaintInteractor;
import naaqll.com.naql.interactor.IComplaintInteractor;
import naaqll.com.naql.model.Compliant;

public class AddCompliantController extends BaseController implements RequestCallback {
    private IComplaintInteractor interactor;

    public AddCompliantController(Context context) {
        super(context);
        interactor = new ComplaintInteractor();
    }

    public void sendCompliant(String email, String phone, String title, String compliantStr) {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        showDialog();
        Compliant compliant = new Compliant();
        compliant.setEmail(email);
        compliant.setPhone(phone);
        compliant.setTitle(title);
        compliant.setCompliant(compliantStr);
        interactor.addComplaint(compliant, this);
    }

    @Override
    public void success(Object o) {
        hideDialog();
        showSuccessMessage(getContext().getString(R.string.compliant_sent));
        finishctivity();
    }

    @Override
    public void failed(String msg) {
        hideDialog();
        showMessage(msg);
    }
}
