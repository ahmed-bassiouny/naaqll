package naaqll.com.naql.controller;

import android.content.Context;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.TripInteractor;

public class TripDetailsController extends BaseController {

    private ITripInteractor interactor;

    public TripDetailsController(Context context) {
        super(context);
        interactor = new TripInteractor();
    }

    public void makeTripUnavailable(int tripId, final IResult<Boolean> result){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        interactor.changeStatus(tripId, 0, new RequestCallback() {
            @Override
            public void success(Object o) {
                hideDialog();
                result.result(true);
            }

            @Override
            public void failed(String msg) {
                hideDialog();
                result.result(false);
            }
        });
    }
}
