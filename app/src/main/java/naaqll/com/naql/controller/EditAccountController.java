package naaqll.com.naql.controller;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.R;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.interactor.UserInteractor;
import naaqll.com.naql.model.User;

public class EditAccountController extends BaseController {
    private IUserInteractor interactor;

    public EditAccountController(Context context, Fragment fragment) {
        super(context, fragment);
        interactor = new UserInteractor();
    }
    public void updateOwnerProfile(final User currentUser,String companyName,String companyActivity,
                                   String commercialNumber,String phone,String country,String city){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        currentUser.setCompanyName(companyName);
        currentUser.setCompanyActivity(companyActivity);
        currentUser.setCommercialNumber(commercialNumber);
        currentUser.setPhone(phone);
        currentUser.setCountry(country);
        currentUser.setCity(city);
        interactor.editProfile(currentUser, new RequestCallback<User>() {
            @Override
            public void success(User user) {
                user.setUserType(currentUser.getUserType());
                user.setLang(currentUser.getLang());
                SharedPrefManager.setObject(Constants.USER,user);
                showSuccessMessage(getContext().getString(R.string.profile_updated));
                hideDialog();
            }

            @Override
            public void failed(String msg) {
                showErrorMessage(msg);
                hideDialog();
            }
        });
    }
    public void updateDriverProfile(final User currentUser, String companyName,
                                    String commercialNumber, String phone,String country,String city){
        showDialog();
        currentUser.setCompanyName(companyName);
        currentUser.setCommercialNumber(commercialNumber);
        currentUser.setPhone(phone);
        currentUser.setCountry(country);
        currentUser.setCity(city);
        interactor.editProfile(currentUser, new RequestCallback<User>() {
            @Override
            public void success(User user) {
                user.setUserType(currentUser.getUserType());
                user.setLang(currentUser.getLang());
                SharedPrefManager.setObject(Constants.USER,user);
                showSuccessMessage(getContext().getString(R.string.profile_updated));
                hideDialog();
            }

            @Override
            public void failed(String msg) {
                showErrorMessage(msg);
                hideDialog();
            }
        });
    }
    public void updatePassword(String oldPassword,String password){
        showDialog();
        interactor.editPassword(password, oldPassword, new RequestCallback() {
            @Override
            public void success(Object o) {
                showSuccessMessage(getContext().getString(R.string.password_updated));
                hideDialog();
                getMyActivity().onBackPressed();
            }

            @Override
            public void failed(String msg) {
                showErrorMessage(msg);
                hideDialog();
            }
        });
    }
}
