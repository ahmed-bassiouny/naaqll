package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.GeneralInteractor;
import naaqll.com.naql.interactor.IGeneralInteractor;
import naaqll.com.naql.model.WebPageType;

public class AboutUsController extends BaseController implements RequestCallback<String> {
    private IGeneralInteractor interactor;
    private IResult<String> result;

    public AboutUsController(Context context, Fragment fragment,IResult<String> result) {
        super(context, fragment);
        interactor = new GeneralInteractor();
        this.result = result;
    }

    public void getData(WebPageType type) {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        getFragment().startLoading();
        if(type == WebPageType.TERMS_CONDITIONS)
            interactor.getTermsConditions(this);
        else
            interactor.aboutUs(this);
    }

    @Override
    public void success(String s) {
        getFragment().endLoading();
        result.result(s);
    }

    @Override
    public void failed(String msg) {
        getFragment().endLoading(msg);
    }
}
