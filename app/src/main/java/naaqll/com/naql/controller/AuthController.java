package naaqll.com.naql.controller;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.AuthActivity;
import naaqll.com.naql.activities.SplashActivity;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.fragments.RegisterFragment;
import naaqll.com.naql.fragments.ResetPasswordFragment;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.LocalApp;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.interactor.UserInteractor;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;


public class AuthController extends BaseController {

    private User user;
    private IUserInteractor interactor;

    public AuthController(Context context) {
        super(context);
        user = new User();
    }

    public AuthController(Context context, Fragment fragment) {
        super(context, fragment);
        this.user = SharedPrefManager.getObject(Constants.USER, User.class);
    }

    public void selectEnglish() {
        user.setLang(Constants.ENG);
        saveUserAndLunchActivity();
    }

    public void selectArabic() {
        user.setLang(Constants.ARA);
        LocalApp.init(getContext(),user.getLang());
        SharedPrefManager.setObject(Constants.USER, user);
        Toast.makeText(getMyActivity(), "Please Wait", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mStartActivity = new Intent(getMyActivity(), SplashActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(getMyActivity(), mPendingIntentId, mStartActivity,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager) getMyActivity().getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                System.exit(0);

            }
        }, 1000);


        /*Intent refresh = new Intent(getMyActivity(), SplashActivity.class);
        refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getMyActivity().startActivity(refresh);
        getMyActivity().finish();*/

        /*Intent mStartActivity = new Intent(getMyActivity(), SplashActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getMyActivity(), mPendingIntentId, mStartActivity,
                PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getMyActivity().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);*/
    }

    private void saveUserAndLunchActivity() {
        SharedPrefManager.setObject(Constants.USER, user);
        launchActivityWithFinish(AuthActivity.class);
    }

    public void setUserType(UserType type) {
        if(user == null)
            user = new User();
        user.setUserType(type);
        SharedPrefManager.setObject(Constants.USER, user);
        MyFragmentTransaction.open(getMyActivity(), new RegisterFragment(), R.id.main_frame, "choose");
    }

    public void forgetPassword(String email) {
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        interactor = new UserInteractor();
        showDialog();
        interactor.forgetPassword(email, new RequestCallback() {
            @Override
            public void success(Object o) {
                showSuccessMessage(getContext().getString(R.string.email_send));
                hideDialog();
                MyFragmentTransaction.open(getMyActivity(), new ResetPasswordFragment(),R.id.main_frame,"forget");
            }

            @Override
            public void failed(String msg) {
                showErrorMessage(msg);
                hideDialog();
            }
        });
    }
}
