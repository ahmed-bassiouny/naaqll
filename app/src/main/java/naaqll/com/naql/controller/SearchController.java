package naaqll.com.naql.controller;

import android.content.Context;

import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.TripInteractor;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

public class SearchController extends BaseController {

    private ITripInteractor interactor;

    public SearchController(Context context) {
        super(context);
        interactor = new TripInteractor();
    }

    public void search(String tripId, String from, String to, final IResult<List<Trip>> result){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        showDialog();
        User user = SharedPrefManager.getObject(Constants.USER,User.class);
        String url;
        if (user.getUserType() == UserType.DRIVER_COMPANIES){
            url = "orders/search/send/drivers";
        }else {
            url = "orders/search";
        }
        interactor.searchTrip(url,tripId, from, to, new RequestCallback<BaseList<Trip>>() {
            @Override
            public void success(BaseList<Trip> tripBaseList) {
                hideDialog();
                result.result(tripBaseList.getDataList());
            }

            @Override
            public void failed(String msg) {
                hideDialog();
                showErrorMessage(msg);
            }
        });
    }
}
