package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.interactor.UserInteractor;

public class MyProfileController extends BaseController {

    private IUserInteractor interactor;

    public MyProfileController(Context context, Fragment fragment) {
        super(context, fragment);
        interactor = new UserInteractor();
    }

    public void getPoint(final IResult<String> result) {
        if (networkAvailable()) {
            interactor.getPoints(new RequestCallback<String>() {
                @Override
                public void success(String s) {
                    result.result(s);
                }

                @Override
                public void failed(String msg) {

                }
            });
        }
    }
}
