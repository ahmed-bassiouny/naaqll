package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.interactor.CarInteractor;
import naaqll.com.naql.interactor.DriverInteractor;
import naaqll.com.naql.interactor.ICarInteractor;
import naaqll.com.naql.interactor.IDriverInteractor;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.User;

public class CarController extends BaseController {
    private ICarInteractor interactor;
    private IResult result;


    public CarController(Context context, IResult result) {
        super(context);
        interactor = new CarInteractor();
        this.result = result;
    }

    public CarController(Context context, Fragment fragment, IResult result) {
        super(context, fragment);
        interactor = new CarInteractor();
        this.result = result;
    }


    public void getCars() {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        if (getFragment() != null)
            getFragment().startLoading();
        else
            showDialog();
        interactor.getCars(callbackList);
    }


    public void addCar(Car car) {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        showDialog();
        interactor.addCar(car, callback);
    }

    public void editCar(Car car) {
        showDialog();
        interactor.editCar(car, callback);

    }

    private RequestCallback<Car> callback = new RequestCallback<Car>() {
        @Override
        public void success(Car car) {
            hideDialog();
            getFragment().endLoading();
            result.result(car);
        }

        @Override
        public void failed(String msg) {
            showMessage(msg);
            hideDialog();
        }
    };

    private RequestCallback<List<Car>> callbackList = new RequestCallback<List<Car>>() {
        @Override
        public void success(List<Car> cars) {
            if(getFragment() != null)
            getFragment().endLoading();
            else hideDialog();
            result.result(cars);
        }

        @Override
        public void failed(String msg) {
            showErrorMessage(msg);
            if(getFragment() != null)
                getFragment().endLoading();
            else hideDialog();
        }
    };


}
