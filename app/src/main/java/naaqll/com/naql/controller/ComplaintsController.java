package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ComplaintInteractor;
import naaqll.com.naql.interactor.IComplaintInteractor;
import naaqll.com.naql.model.Compliant;

public class ComplaintsController extends BaseController implements RequestCallback<List<Compliant>> {

    private IComplaintInteractor interactor;
    private IResult<List<Compliant>> result;

    public ComplaintsController(Context context, Fragment fragment) {
        super(context, fragment);
        interactor = new ComplaintInteractor();
    }
    public void getComplaints(IResult<List<Compliant>> result){
        if(!networkAvailable()){
            showAlertConnection();
            return;
        }
        getFragment().startLoading();
        this.result = result;
        interactor.getComplaints(this);
    }

    @Override
    public void success(List<Compliant> compliants) {
        result.result(compliants);
        getFragment().endLoading();
    }

    @Override
    public void failed(String msg) {
        getFragment().endLoading(msg);
    }
}
