package naaqll.com.naql.controller;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.List;
import java.util.Locale;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.TripInteractor;
import naaqll.com.naql.model.Trip;

public class OwnerOrdersController extends BaseController implements RequestCallback<BaseList<Trip>> {

    private ITripInteractor iTripInteractor;
    private IResult<List<Trip>> result;

    public OwnerOrdersController(Context context, Fragment fragment) {
        super(context, fragment);
        iTripInteractor = new TripInteractor();
    }

    public void getCurrentTripList(IResult<List<Trip>> result) {
        if (networkAvailable()) {
            getFragment().startLoading();
            this.result = result;
            String status = String.format(Locale.ENGLISH,"%d",Trip.RUNNING);
            iTripInteractor.list(0,50,status, this);
        }
    }

    @Override
    public void success(BaseList<Trip> tripBaseList) {
        getFragment().endLoading();
        result.result(tripBaseList.getDataList());
    }

    @Override
    public void failed(String msg) {
        getFragment().endLoading(msg);
    }
}
