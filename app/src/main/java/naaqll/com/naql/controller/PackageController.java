package naaqll.com.naql.controller;

import android.content.Context;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.interactor.GeneralInteractor;
import naaqll.com.naql.interactor.IGeneralInteractor;
import naaqll.com.naql.model.PackageTrip;

public class PackageController extends BaseController implements RequestCallback<List<PackageTrip>> {

    private IGeneralInteractor interactor;
    private IResult<List<PackageTrip>> result;

    public PackageController(Context context) {
        super(context);
        interactor = new GeneralInteractor();
    }

    public void getPackage(IResult<List<PackageTrip>> result) {
        if (!networkAvailable()) {
            showAlertConnection();
            return;
        }
        showDialog();
        this.result = result;
        interactor.getPackageTrip(this);
    }

    @Override
    public void success(List<PackageTrip> packageTrips) {
        result.result(packageTrips);
        hideDialog();
    }

    @Override
    public void failed(String msg) {
        showErrorMessage(msg);
        hideDialog();
    }
}
