package naaqll.com.naql.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import bassiouny.ahmed.genericmanager.CustomNotificationManager;
import naaqll.com.naql.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        CustomNotificationManager notify = CustomNotificationManager.getInstance(this);
        if(remoteMessage == null || remoteMessage.getNotification() == null)
            return;
        notify.setTitle(remoteMessage.getNotification().getTitle());
        notify.setBody(remoteMessage.getNotification().getBody());
        notify.setIcon(R.mipmap.ic_launcher);
        notify.show(1);
    }

}
