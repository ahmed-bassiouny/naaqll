package naaqll.com.naql.helper;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.ArrayRes;
import android.widget.EditText;

import naaqll.com.naql.R;

public class Utils {
    public static String checkString(String msg) {
        if (msg == null)
            msg = "";
        return msg;
    }

    public static boolean validEditText(EditText text){
        if(text.getText().toString().trim().isEmpty()) {
            text.setError(text.getContext().getString(R.string.required));
            return false;
        }
        return true;
    }

    public static String getStringFromArrayByIndex(Context context, @ArrayRes int array, int index){
        return context.getResources().getStringArray(array)[index];
    }
}
