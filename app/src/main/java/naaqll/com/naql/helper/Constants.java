package naaqll.com.naql.helper;

public interface Constants {
    String TITLE = "title";
    String USER = "user";
    String ENG = "en";
    String ARA = "ar";
    String DATE_TIME_FORMATE = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMATE = "yyyy MMM dd";
    String DRIVER = "driver";
    String TYPE = "type";
    String LOCATION = "location";
    int TRIP_ID_START_FROM = 0;
    String NOTIFICATION = "notification";
    String FROM_TIME = "from_time";
    String TO_TIME = "to_time";
}
