package naaqll.com.naql.helper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.Locale;

public class LocalApp {

    public static void init(Context context, String lan){
        Locale myLocale = new Locale(lan);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        MyApplication.LANGUAGE = lan;
    }
}
