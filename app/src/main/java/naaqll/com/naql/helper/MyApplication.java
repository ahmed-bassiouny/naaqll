package naaqll.com.naql.helper;

import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Locale;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.api.RetrofitConfig;
import naaqll.com.naql.model.User;

public class MyApplication extends Application {

    public static String LANGUAGE = "en";

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPrefManager.init(this, "Owner");
        RetrofitConfig.initRetrofitConfig();
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        if (user != null) {
            // change language
            LANGUAGE = user.getLang();
            LocalApp.init(this, user.getLang());
        }
    }
}
