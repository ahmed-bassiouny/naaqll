package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.CompanyOrDriver;

public class CompanyOrDriverAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CompanyOrDriver> list;
    private Context context;

    public CompanyOrDriverAdapter(Context context, List<CompanyOrDriver> list) {
        this.list = list;
        this.context = context;
    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_company_name)
        TextView tvCompanyName;
        @BindView(R.id.tv_company_shipment)
        TextView tvCompanyShipment;

        public CompanyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class DriverViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_driver_name)
        TextView tvDriverName;
        @BindView(R.id.tv_phone)
        TextView tvPhone;
        @BindView(R.id.tv_car_type) TextView tvCarType;
        @BindView(R.id.tv_model) TextView tvModel;
        @BindView(R.id.tv_plate_number) TextView tvPlateNumber;
        @BindView(R.id.tv_color) TextView tvColor;
//        @BindView(R.id.tv_form_number) TextView tvFormNumber;
//        @BindView(R.id.tv_form_expiration_date) TextView tvFormExpirationDate;

        public DriverViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 0) {
            // company
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_company_have_drivers, parent, false);
            return new CompanyViewHolder(itemView);
        } else {
            // driver
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_driver, parent, false);
            return new DriverViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        CompanyOrDriver item = list.get(position);
        if (item.isCompany()) {
            CompanyViewHolder holder = (CompanyViewHolder) viewHolder;
            holder.tvCompanyName.setText(item.getCompanyName());
            holder.tvCompanyShipment.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.current_shipments), item.getCompanyShipment()));
        } else {
            DriverViewHolder holder = (DriverViewHolder) viewHolder;
            holder.tvDriverName.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.driver_name), item.getDriverName()));
            holder.tvPhone.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.phone), item.getDriverPhone()));
            holder.tvCarType.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.car_type), item.getCarType()));
            holder.tvModel.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.model), item.getModel()));
            holder.tvPlateNumber.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.plate_number), item.getPlateNumber()));

            holder.tvColor.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.color), item.getColor()));
            //holder.tvFormNumber.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.form_number), item.getFormNumber()));
            //holder.tvFormExpirationDate.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.form_expiration_date), item.getFormExpirationDate()));

        }
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        CompanyOrDriver item = list.get(position);
        if (item.isCompany()) {
            return 0;
        } else {
            return 1;
        }
    }
}
