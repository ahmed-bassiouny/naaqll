package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Trip;

public class TripForOwnerAdapter extends RecyclerView.Adapter<TripForOwnerAdapter.MyViewHolder> {

    private List<Trip> list;
    private Context context;
    private IAdapter<Trip> iAdapter;

    public TripForOwnerAdapter(Context context, IAdapter<Trip> iAdapter) {
        this.list = new ArrayList<>();
        this.context = context;
        this.iAdapter = iAdapter;
    }

    /*public TripForOwnerAdapter(Context context, IAdapter<Trip> iAdapter, boolean hideStatue) {
        this.list = new ArrayList<>();
        this.context = context;
        this.iAdapter = iAdapter;
        this.hideStatue = hideStatue;
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_order_type)
        TextView tvOrderType;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_from_city)
        TextView tvFromCity;
        @BindView(R.id.tv_from_address)
        TextView tvFromAddress;
        @BindView(R.id.tv_to_city)
        TextView tvToCity;
        @BindView(R.id.tv_to_address)
        TextView tvToAddress;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_shipment)
        TextView tvShipment;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iAdapter.click(getAdapterPosition(), list.get(getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trip_for_owner, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Trip item = list.get(position);
        holder.tvOrderType.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.products_type),
                context.getResources().getStringArray(R.array.products_type)[item.getOrderType()]));
        holder.tvDate.setText(item.getDateTrip());
        holder.tvFromCity.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.from_city), item.getFromCity()));
        holder.tvFromAddress.setText(item.getFromAddress());
        holder.tvToCity.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.to_city), item.getToCity()));
        holder.tvToAddress.setText(item.getToAddress());
        holder.price.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.price), item.getPrice()));
        /*if (hideStatue) {
            holder.tvStatus.setVisibility(View.GONE);
        } else {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText(context.getString(item.getStatusString()));
            holder.tvStatus.setBackgroundColor(context.getResources().getColor(item.getStatusColor()));
        }*/
        holder.tvShipment.setText(context.getString(R.string.shipments) + " : " + (item.getNumberOfShipments() - item.getDriversCount()));

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public void setList(List<Trip> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void updateStatusToCancel(int position) {
        Trip trip = list.get(position);
        trip.setStatus(Trip.CANCELED);
        list.set(position, trip);
        notifyItemChanged(position);
    }

    public void insertItem(Trip trip) {
        list.add(0, trip);
        notifyItemInserted(0);
    }
}
