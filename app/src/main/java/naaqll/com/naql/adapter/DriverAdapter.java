package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.Driver;

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.MyViewHolder> {

    private List<Driver> list;
    private Context context;
    private IDriverAdapter interfaceAdapter;

    public DriverAdapter(Context context, IDriverAdapter interfaceAdapter) {
        this.list = new ArrayList<>();
        this.context = context;
        this.interfaceAdapter = interfaceAdapter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_driver_name)
        TextView tvDriverName;
        @BindView(R.id.tv_phone)
        TextView tvPhone;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interfaceAdapter.editDriver(getAdapterPosition(), list.get(getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_driver_basic, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Driver item = list.get(position);
        holder.tvDriverName.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.driver_name), item.getName()));
        holder.tvPhone.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.phone), item.getPhone()));

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public void addList(List<Driver> list) {
        this.list = list;
    }

    public void update(int position, Driver driver) {
        this.list.set(position, driver);
        notifyItemChanged(position);
    }

    public void delete(int position) {
        this.list.remove(position);
        notifyItemRemoved(position);
    }

    public void add(Driver driver) {
        this.list.add(driver);
        notifyItemInserted(getItemCount() - 1);
    }

}
