package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.Driver;

public class DriverInTripAdapter extends RecyclerView.Adapter<DriverInTripAdapter.MyViewHolder> {

    private List<Driver> list;
    private IAdapter adapter;

    public DriverInTripAdapter(IAdapter adapter) {
        this.list = new ArrayList<>();
        this.adapter = adapter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.im_delete)
        ImageView imDelete;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            imDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeItem(getAdapterPosition());
                    adapter.click(getAdapterPosition(),null);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_driver_in_trip, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Driver item = list.get(position);
        holder.tvName.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public void addItem(Driver driver) {
        list.add(0, driver);
        notifyItemInserted(0);
    }

    private void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public List<Driver> getList() {
        return list;
    }
}
