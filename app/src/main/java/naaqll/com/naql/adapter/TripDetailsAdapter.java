package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Compliant;

public class TripDetailsAdapter extends RecyclerView.Adapter<TripDetailsAdapter.MyViewHolder> {

    private String[] question;
    private String[] answer;
    private Context context;

    public TripDetailsAdapter(Context context,String[] question, String[] answer) {
        this.context = context;
        this.question = question;
        this.answer = answer;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_question)
        TextView tvQuestion;
        @BindView(R.id.tv_answer)
        TextView tvAnswer;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trip_details, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(question[position].equals(context.getResources().getString(R.string.from_city)) || question[position].equals(context.getResources().getString(R.string.to_city)) || question[position].equals(context.getResources().getString(R.string.price))
                || question[position].equals(context.getResources().getString(R.string.from_city))){
            holder.tvQuestion.setTextColor(context.getResources().getColor(R.color.light_black));
            holder.tvAnswer.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }else {
            holder.tvQuestion.setTextColor(context.getResources().getColor(R.color.black));
            holder.tvAnswer.setTextColor(context.getResources().getColor(R.color.light_blue));
        }
        holder.tvQuestion.setText(question[position]);
        holder.tvAnswer.setText(answer[position]);
    }

    @Override
    public int getItemCount() {
        if (question == null)
            return 0;
        return question.length;
    }
    public void changeDriverCount(int count){
        answer[answer.length-1] = String.valueOf(count);
        notifyItemChanged(answer.length-1);
    }
}
