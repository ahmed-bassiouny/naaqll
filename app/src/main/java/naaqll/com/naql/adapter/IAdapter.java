package naaqll.com.naql.adapter;

import naaqll.com.naql.model.Driver;

public interface IAdapter<T> {
    void click(int position, T t);
}
