package naaqll.com.naql.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Driver;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.MyViewHolder> {

    private List<Car> list;
    private Context context;
    private IAdapter<Car> interfaceAdapter;

    public CarAdapter(Context context, IAdapter<Car> interfaceAdapter) {
        this.list = new ArrayList<>();
        this.context = context;
        this.interfaceAdapter = interfaceAdapter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_car_type) TextView tvCarType;
        @BindView(R.id.tv_model) TextView tvModel;
        @BindView(R.id.tv_plate_number) TextView tvPlateNumber;
        @BindView(R.id.tv_color) TextView tvColor;
//        @BindView(R.id.tv_form_number) TextView tvFormNumber;
//        @BindView(R.id.tv_form_expiration_date) TextView tvFormExpirationDate;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interfaceAdapter.click(getAdapterPosition(), list.get(getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Car item = list.get(position);
        holder.tvCarType.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.car_type), item.getCarType()));
        holder.tvModel.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.model), item.getModel()));
        holder.tvPlateNumber.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.plate_number), item.getPlateNumber()));

        holder.tvColor.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.color), item.getColor()));
        //holder.tvFormNumber.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.form_number), item.getFormNumber()));
        //holder.tvFormExpirationDate.setText(String.format(Locale.getDefault(), "%s : %s", context.getString(R.string.form_expiration_date), item.getFormExpirationDate()));


    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public void addList(List<Car> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void update(int position, Car car) {
        this.list.set(position, car);
        notifyItemChanged(position);
    }

    public void add(Car car) {
        this.list.add(car);
        notifyItemInserted(getItemCount() - 1);
    }

}
