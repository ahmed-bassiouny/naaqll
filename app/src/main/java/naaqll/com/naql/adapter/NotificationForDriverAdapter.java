package naaqll.com.naql.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.Notification;

public class NotificationForDriverAdapter extends RecyclerView.Adapter<NotificationForDriverAdapter.MyViewHolder> {

    private List<Notification> list;

    public NotificationForDriverAdapter(List<Notification> list) {
        this.list = list;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.check)
        CheckBox check;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    Notification item = list.get(getAdapterPosition());
                    item.setSelected(b);
                    list.set(getAdapterPosition(),item);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification_for_driver, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Notification item = list.get(position);

        holder.check.setText(item.getName());
        if(item.isSelected()){
            holder.check.setChecked(true);
        }else {
            holder.check.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }

    public List<Notification> getList() {
        return list;
    }

    public void updateFiled(int position){
        Notification item = list.get(position);
        item.setSelected(true);
        list.set(position,item);
        notifyItemChanged(position);
    }
}
