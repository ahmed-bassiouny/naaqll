package naaqll.com.naql.adapter;

import naaqll.com.naql.model.Driver;

public interface IDriverAdapter {
    void editDriver(int position,Driver driver);
    void changeStatusDriver(int position,Driver driver);
}
