package naaqll.com.naql.base.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.tapadoo.alerter.Alerter;

import dmax.dialog.SpotsDialog;
import naaqll.com.naql.R;
import naaqll.com.naql.helper.Utils;


public abstract class BaseController implements IBaseController {

    private IFragment iFragment;
    private Context context;

    private AlertDialog dialog;


    public BaseController(Context context, Fragment fragment) {
        this.context = context;
        iFragment = (IFragment) fragment;
    }

    public BaseController(Context context) {
        this.context = context;
    }


    public void showDialog() {
        if (dialog == null)
            dialog = new SpotsDialog.Builder().setContext(context).build();
        if (!dialog.isShowing())
            dialog.show();
    }

    public void hideDialog() {
        if (dialog == null)
            return;
        dialog.hide();
    }

    public boolean networkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public interface IResult<T> {
        void result(T t);
    }

    public interface IResultWithTotalItem<T> {
        void result(T t);

        void totalItemCount(int total);
    }

    public IFragment getFragment() {
        return iFragment;
    }

    public Context getContext() {
        return context;
    }

    public void launchActivity(Class<?> myClass) {
        context.startActivity(new Intent(context, myClass));
    }

    public void launchActivityForResult(Class<?> myClass, int requestCode) {
        getMyActivity().startActivityForResult(new Intent(context, myClass), requestCode);
    }

    public void launchActivityWithFinish(Class<?> myClass) {
        context.startActivity(new Intent(context, myClass));
        finishctivity();
    }

    public void launchActivityWithFinishAndClearStack(Class<?> myClass) {
        Intent intent = new Intent(context, myClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        finishctivity();
    }

    public void finishctivity() {
        ((Activity) context).finish();
    }

    public void launchActivity(Class<?> myClass, Bundle bundle) {
        Intent intent = new Intent(context, myClass);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public AppCompatActivity getMyActivity() {
        return (AppCompatActivity) context;
    }


    @Override
    public void showMessage(String msg) {
       /* Alerter.create((Activity) context)
                .setTitle(getContext().getString(R.string.app_name))
                .setText(Utils.checkString(msg))
                .setBackgroundColorRes(R.color.colorAccent)
                .show();*/
        Toast.makeText(context, Utils.checkString(msg), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSuccessMessage(String msg) {
       /* Alerter.create((Activity) context)
                .setTitle(getContext().getString(R.string.app_name))
                .setText(Utils.checkString(msg))
                .setBackgroundColorRes(R.color.green)
                .show();*/
        Toast.makeText(context, Utils.checkString(msg), Toast.LENGTH_LONG).show();
    }


    @Override
    public void showAlertConnection() {
      /*  Alerter.create((Activity) context)
                .setTitle("Ops")
                .setText("no internet connection")
                //.setIcon(R.drawable.no_connection)
                .setBackgroundColorRes(R.color.red)
                .show();*/
        Toast.makeText(context, Utils.checkString("no internet connection"), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(String msg) {
       /* Alerter.create((Activity) context)
                .setTitle(getContext().getString(R.string.app_name))
                .setText(Utils.checkString("klfdmblkd"))
                .setBackgroundColorRes(R.color.red)
                .show();*/
        Toast.makeText(context, Utils.checkString(msg), Toast.LENGTH_LONG).show();
    }


    @Override
    public void showAlertConnectionWithAction(String msg, View.OnClickListener listener) {
        Alerter.create((Activity) context)
                .setTitle(getContext().getString(R.string.app_name))
                .setText(Utils.checkString(msg))
                .setBackgroundColorRes(R.color.colorAccent)
                .addButton("Try Again", R.style.AlertButton, listener)
                .show();
    }

}
