package naaqll.com.naql.base.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.AuthActivity;
import naaqll.com.naql.activities.SplashActivity;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.MyApplication;


public abstract class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;

    ImageView ivNotification;
    ImageView ivBack;
    TextView tvTitle;
    Button ivLogout;

    public void initToolbar(@IdRes int layout) {
        if (MyApplication.LANGUAGE.equals(Constants.ARA)) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_RTL);
            Log.e( "initToolbar: ", "ar");
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_LTR);
            Log.e( "initToolbar: ", "en");
        }
        toolbar = findViewById(layout);

        ivNotification = findViewById(R.id.iv_notification);
        ivBack = findViewById(R.id.iv_back);
        tvTitle = findViewById(R.id.tv_title);
        ivLogout = findViewById(R.id.iv_logout);
        ivNotification.setVisibility(View.GONE);
        if (isTaskRoot()) {
            //ivLogout.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.INVISIBLE);
        } else {
            //ivLogout.setVisibility(View.GONE);
            ivBack.setVisibility(View.VISIBLE);
        }
        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
                builder.setMessage(getString(R.string.are_you_sure));
                builder.setCancelable(true);

                builder.setPositiveButton(
                        getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPrefManager.clearSharedPref();
                                dialog.cancel();
                                finish();
                                startActivity(new Intent(BaseActivity.this, SplashActivity.class));
                            }
                        });

                builder.setNegativeButton(getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    public Toolbar getToolbar() {
        return this.toolbar;
    }

    public void hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    protected void setTitle(String title) {
        tvTitle.setText(title);
    }
}
