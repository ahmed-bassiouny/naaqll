package naaqll.com.naql.base.ui;

import android.view.View;

public interface IFragment {
    void startLoading();
    void endLoading(String error);
    void endLoading();
}
