package naaqll.com.naql.base.ui;

public interface IAdapter<T> {
    void onClick(T t,int position);
}
