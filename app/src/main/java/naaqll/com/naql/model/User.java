package naaqll.com.naql.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import naaqll.com.naql.helper.Utils;
import okhttp3.internal.Util;

public class User {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_activity")
    @Expose
    private String companyActivity;
    @SerializedName("commercial_number")
    @Expose
    private String commercialNumber;
    @SerializedName("commercial_date")
    @Expose
    private String commercialDate;
    private String lang;
    @SerializedName("type")
    @Expose
    private String userType; // 1 for factories 2 for driver companies
    @SerializedName("notification_token")
    @Expose
    private String notificationToken;
    @SerializedName("is_admin")
    @Expose
    private Boolean isAdmin;
    @SerializedName("is_approved")
    @Expose
    private Boolean isApproved;
    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("reset_password_code")
    @Expose
    private Object resetPasswordCode;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("license_name")
    @Expose
    private String licenseName;
    @SerializedName("license_number")
    @Expose
    private String licenseNumber;
    @SerializedName("license_date")
    @Expose
    private String licenseDate;
    @SerializedName("tax_number")
    @Expose
    private String taxNumber;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyActivity() {
        return companyActivity;
    }

    public void setCompanyActivity(String companyActivity) {
        this.companyActivity = companyActivity;
    }

    public String getCommercialNumber() {
        return commercialNumber;
    }

    public void setCommercialNumber(String commercialNumber) {
        this.commercialNumber = commercialNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Object getResetPasswordCode() {
        return resetPasswordCode;
    }

    public void setResetPasswordCode(Object resetPasswordCode) {
        this.resetPasswordCode = resetPasswordCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getLang() {
        return Utils.checkString(lang);
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserType(UserType userType) {
        if (userType == UserType.FACTORIES)
            this.userType = "1";
        else
            this.userType = "2";
    }

    public UserType getUserType() {
        return Utils.checkString(userType).equals("1") ?UserType.FACTORIES:UserType.DRIVER_COMPANIES;
    }

    public String getPoints() {
        return Utils.checkString(points);
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public void setCommercialDate(String commercialDate) {
        this.commercialDate = commercialDate;
    }

    public void setLicenseName(String licenseName) {
        this.licenseName = licenseName;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public void setLicenseDate(String licenseDate) {
        this.licenseDate = licenseDate;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }
}
