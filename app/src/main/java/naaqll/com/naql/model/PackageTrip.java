package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

import naaqll.com.naql.helper.Utils;

public class PackageTrip {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("number_of_chipment")
    private String numberOfTrip;
    @SerializedName("price")
    private String packageTrip;

    public String getName() {
        return Utils.checkString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Utils.checkString(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumberOfTrip() {
        return Utils.checkString(numberOfTrip);
    }

    public void setNumberOfTrip(String numberOfTrip) {
        this.numberOfTrip = numberOfTrip;
    }

    public String getPackageTrip() {
        return Utils.checkString(packageTrip);
    }

    public void setPackageTrip(String packageTrip) {
        this.packageTrip = packageTrip;
    }
}
