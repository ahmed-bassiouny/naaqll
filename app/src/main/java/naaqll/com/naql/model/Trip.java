package naaqll.com.naql.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import bassiouny.ahmed.genericmanager.DateTimeManager;
import naaqll.com.naql.R;
import naaqll.com.naql.helper.Utils;

public class Trip implements Serializable {

    public final static int CANCELED = 0;
    public final static int RUNNING = 1;

    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("from_city")
    @Expose
    private String fromCity;
    @SerializedName("to_city")
    @Expose
    private String toCity;
    @SerializedName("return_city")
    @Expose
    private String returnCity;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("order_type")
    @Expose
    private String orderType;
    @SerializedName("trip_type")
    @Expose
    private String tripType;
    @SerializedName("number_of_shipments_available")
    @Expose
    private String numberOfShipmentsAvailable;
    @SerializedName("Load_in_ton")
    @Expose
    private String loadInTon;
    @SerializedName("from_lng")
    @Expose
    private double fromLng;
    @SerializedName("from_lat")
    @Expose
    private double fromLat;
    @SerializedName("from_address")
    @Expose
    private String fromAddress;
    @SerializedName("to_lng")
    @Expose
    private double toLng;
    @SerializedName("to_lat")
    @Expose
    private double toLat;
    @SerializedName("to_address")
    @Expose
    private String toAddress;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("companies")
    private List<DriversCompany> driversCompanies;
    @SerializedName("drivers")
    private List<Driver> driver;
    @SerializedName("drivers_count")
    private int driversCount;
    @SerializedName("from_time")
    private String fromTime;
    @SerializedName("to_time")
    private String toTime;
    @SerializedName("holiday")
    private String holiday;

    @SerializedName("return_lng")
    @Expose
    private double returnLng;
    @SerializedName("return_lat")
    @Expose
    private double returnLat;
    @SerializedName("return_address")
    @Expose
    private String returnAddress;
    @SerializedName("topic")
    @Expose
    private int topic;
    @SerializedName("save_cost")
    @Expose
    private String saveCost;
    @SerializedName("requires_seaport")
    @Expose
    private String enterPort;
    @SerializedName("type_of_load")
    @Expose
    private String typeOfLoad;
    @SerializedName("desc_of_load")
    @Expose
    private String descOfLoad;
    @SerializedName("payment_method")
    @Expose
    private int paymentMethod;


    public Trip(String country,
                String fromCity, String fromAddress, double fromLat, double fromLng,
                String toCity, String toAddress, double toLat, double toLng,
                String companyName,
                String orderType, int tripTypeIndex, String numberOfShipmentsAvailable, String loadInTon,
                String price, String fromTime, String toTime, String holiday, int topic, int saveCostIndex,
                boolean enterPort,String typeOfLoad,String descOfLoad,int paymentMethod) {
        this.country = country;
        this.fromCity = fromCity;
        this.fromAddress = fromAddress;
        this.fromLat = fromLat;
        this.fromLng = fromLng;
        this.toCity = toCity;
        this.toAddress = toAddress;
        this.toLat = toLat;
        this.toLng = toLng;
        this.companyName = companyName;
        this.orderType = orderType;
        this.tripType = String.valueOf(tripTypeIndex);
        this.numberOfShipmentsAvailable = numberOfShipmentsAvailable;
        this.loadInTon = loadInTon;
        this.price = price;
        this.status = 1;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.holiday = holiday;
        this.topic = topic;
        this.saveCost = String.valueOf(saveCostIndex);
        this.enterPort = enterPort ? "1" : "0";
        this.typeOfLoad = typeOfLoad;
        this.descOfLoad = descOfLoad;
        this.paymentMethod = paymentMethod;
    }

    public Trip() {
    }

    public String getCountry() {
        return Utils.checkString(country);
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFromCity() {
        return Utils.checkString(fromCity);
    }

    public String getToCity() {
        return Utils.checkString(toCity);
    }

    public String getCompanyName() {
        return Utils.checkString(companyName);
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getOrderType() {
        orderType = Utils.checkString(orderType);
        try {
            return Integer.parseInt(orderType);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public int getTripType() {
        try {
            return Integer.parseInt(tripType);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getNumberOfShipmentsAvailable() {
        return Utils.checkString(numberOfShipmentsAvailable);
    }

    public int getNumberOfShipments() {
        try {
            return Integer.parseInt(numberOfShipmentsAvailable);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setNumberOfShipmentsAvailable(String numberOfShipmentsAvailable) {
        this.numberOfShipmentsAvailable = numberOfShipmentsAvailable;
    }

    public String getLoadInTon() {
        return Utils.checkString(loadInTon);
    }

    public void setLoadInTon(String loadInTon) {
        this.loadInTon = loadInTon;
    }

    public double getFromLng() {
        return fromLng;
    }

    public void setFromLng(double fromLng) {
        this.fromLng = fromLng;
    }

    public double getFromLat() {
        return fromLat;
    }

    public void setFromLat(double fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromAddress() {
        return Utils.checkString(fromAddress);
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public double getToLng() {
        return toLng;
    }

    public void setToLng(double toLng) {
        this.toLng = toLng;
    }

    public double getToLat() {
        return toLat;
    }

    public void setToLat(double toLat) {
        this.toLat = toLat;
    }

    public String getToAddress() {
        return Utils.checkString(toAddress);
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getPrice() {
        return Utils.checkString(price);
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return Utils.checkString(createdAt);
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateTrip() {
        return DateTimeManager.changeDateFormat(getCreatedAt(), "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd");
    }

    public int getStatusString() {
        switch (status) {
            case CANCELED:
                return R.string.inactive;
            case RUNNING:
                return R.string.active;
        }
        return R.string.inactive;
    }

    public int getStatusColor() {
        switch (status) {
            case CANCELED:
                return R.color.red;
            case RUNNING:
                return R.color.green;
        }
        return R.color.white;
    }

    public List<DriversCompany> getDriversCompanies() {
        if (driversCompanies == null)
            driversCompanies = new ArrayList<>();
        return driversCompanies;
    }

    public String getFromTime() {
        return Utils.checkString(fromTime);
    }

    public String getToTime() {
        return Utils.checkString(toTime);
    }

    public String getHoliday() {
        return holiday;
    }

    public int getDriversCount() {
        return driversCount;
    }

    public void setDriversCount(int driversCount) {
        this.driversCount = driversCount;
    }

    public double getReturnLng() {
        return returnLng;
    }

    public void setReturnLng(double returnLng) {
        this.returnLng = returnLng;
    }

    public double getReturnLat() {
        return returnLat;
    }

    public void setReturnLat(double returnLat) {
        this.returnLat = returnLat;
    }

    public String getReturnAddress() {
        if (returnAddress == null || returnAddress.isEmpty())
            returnAddress = " - ";
        return returnAddress;
    }

    public void setReturnAddress(String returnAddress) {
        this.returnAddress = returnAddress;
    }

    public String getReturnCity() {
        if (returnCity == null || returnCity.isEmpty())
            returnCity = " - ";
        return returnCity;
    }

    public void setReturnCity(String returnCity) {
        this.returnCity = returnCity;
    }

    public int getSaveCost() {
        try {
            return Integer.parseInt(saveCost);
        } catch (Exception e) {
            return 0;
        }
    }

    public List<Driver> getDriver() {
        if (driver == null)
            driver = new ArrayList<>();
        return driver;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }
}
