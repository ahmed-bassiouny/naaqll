package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriversToTripRequest {

    @SerializedName("owner_id")
    private int ownerId;
    @SerializedName("order_id")
    private int order_id;
    @SerializedName("drivers")
    private List<Driver> drivers;

    public DriversToTripRequest(int ownerId, int order_id, List<Driver> drivers) {
        this.ownerId = ownerId;
        this.order_id = order_id;
        this.drivers = drivers;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }
}
