package naaqll.com.naql.model;

import android.location.Address;

import java.io.Serializable;

import naaqll.com.naql.helper.Utils;

public class MyPlace implements Serializable {
    private double lat;
    private double lng;
    private String addressStr;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String knownName;

    public MyPlace(double lat, double lng, Address address) {
        this.lat = lat;
        this.lng = lng;
        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        addressStr = address.getAddressLine(0);
        city = address.getLocality();
        state = address.getAdminArea();
        country = address.getCountryName();
        postalCode = address.getPostalCode();
        knownName = address.getFeatureName();
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddressStr() {
        return Utils.checkString(addressStr);
    }

    public String getCity() {
        return Utils.checkString(city);
    }

    public String getState() {
        return Utils.checkString(state);
    }

    public String getCityOrState() {
        if (!getCity().isEmpty())
            return getCity();
        else return getState();
    }

    public String getCountry() {
        return Utils.checkString(country);
    }

    public String getPostalCode() {
        return Utils.checkString(postalCode);
    }

    public String getKnownName() {
        return Utils.checkString(knownName);
    }
}
