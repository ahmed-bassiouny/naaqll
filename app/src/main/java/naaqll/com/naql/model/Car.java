package naaqll.com.naql.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import naaqll.com.naql.helper.Utils;

public class Car implements Serializable {

    @SerializedName(value = "id")
    private int id;
    @SerializedName(value = "car_id")
    private int carId;
    @SerializedName("plate_number")
    private String plateNumber;
//    @SerializedName("form_number")
//    private String formNumber;
    @SerializedName("color")
    private String color;
    @SerializedName("model")
    private String model;
//    @SerializedName("form_end_date")
//    private String formExpirationDate;
    @SerializedName("car_type")
    private String carType;
    @SerializedName("is_deleted")
    private String deleted;

    public Car() {
    }

    public Car(int id, String plateNumber, String color, String model, String carType) {
        this.id = id;
        this.plateNumber = plateNumber;
        //this.formNumber = formNumber;
        this.color = color;
        this.model = model;
        //this.formExpirationDate = formExpirationDate;
        this.carType = carType;
    }

    public int getId() {
        return id;
    }

    public void setCarId() {
        carId = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlateNumber() {
        return Utils.checkString(plateNumber);
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

//    public String getFormNumber() {
//        return Utils.checkString(formNumber);
//    }
//
//    public void setFormNumber(String formNumber) {
//        this.formNumber = formNumber;
//    }

    public String getColor() {
        return Utils.checkString(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return Utils.checkString(model);
    }

    public void setModel(String model) {
        this.model = model;
    }

//    public String getFormExpirationDate() {
//        return Utils.checkString(formExpirationDate);
//    }
//
//    public void setFormExpirationDate(String formExpirationDate) {
//        this.formExpirationDate = formExpirationDate;
//    }

    public String getCarType() {
        return Utils.checkString(carType);
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public boolean getDeleted() {
        if (Utils.checkString(deleted).equals("0"))
            return false; // 0 active
        else
            return true; // 1 or empty is deleted
    }

    public void setDeleted(boolean status) {
        if (status) {
            deleted = "1";
        } else {
            deleted = "0";
        }
    }
}
