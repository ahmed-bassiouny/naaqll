package naaqll.com.naql.model;

import android.graphics.drawable.Drawable;

public class Option {

    private int id;
    private String name;
    private Drawable drawableImage;

    public Option(int id,String name, Drawable drawableImage) {
        this.id = id;
        this.name = name;
        this.drawableImage = drawableImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getDrawableImage() {
        return drawableImage;
    }

    public void setDrawableImage(Drawable drawableImage) {
        this.drawableImage = drawableImage;
    }

    public int getId() {
        return id;
    }
}
