package naaqll.com.naql.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import bassiouny.ahmed.genericmanager.DateTimeManager;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Utils;

public class Compliant {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("compliant")
    @Expose
    private String compliant;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return Utils.checkString(userId);
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return Utils.checkString(email);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return Utils.checkString(phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTitle() {
        return Utils.checkString(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompliant() {
        return Utils.checkString(compliant);
    }

    public void setCompliant(String compliant) {
        this.compliant = compliant;
    }

    public String getCreatedAt() {
        createdAt = Utils.checkString(createdAt);
        return DateTimeManager.changeDateFormat(createdAt, Constants.DATE_TIME_FORMATE,Constants.DATE_FORMATE);
    }
}

