package naaqll.com.naql.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import naaqll.com.naql.helper.Utils;

public class DriversCompany implements Serializable {

    @SerializedName("company_name")
    private String companyName;
    @SerializedName("drivers")
    private List<Driver> driver;

    public DriversCompany() {
    }

    public DriversCompany(String companyName, List<Driver> driver) {
        this.companyName = companyName;
        this.driver = driver;
    }

    public String getCompanyName() {

        return Utils.checkString(companyName);
    }

    public List<Driver> getDriver() {
        if(driver == null)
            driver = new ArrayList<>();
        return driver;
    }
}
