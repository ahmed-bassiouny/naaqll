package naaqll.com.naql.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import naaqll.com.naql.helper.Utils;

public class Driver implements Serializable{
    @SerializedName(value = "id")
    @Expose
    private int id;
    @SerializedName("driver_id")
    @Expose
    private int driverId;
    @SerializedName("owner_id")
    @Expose
    private String ownerId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("is_active")
    @Expose
    private int isActive;

    @SerializedName("car_id")
    private int carId;
    @SerializedName("plate_number")
    private String plateNumber;
//    @SerializedName("form_number")
//    private String formNumber;
    @SerializedName("color")
    private String color;
    @SerializedName("model")
    private String model;
//    @SerializedName("form_end_date")
//    private String formExpirationDate;
    @SerializedName("car_type")
    private String carType;

    public Driver() {
    }

    public Driver(Car car, String name, String phone) {
        this.carId = car.getId();
        this.plateNumber = car.getPlateNumber();
        //this.formNumber = car.getFormNumber();
        this.color = car.getColor();
        this.model = car.getModel();
        //this.formExpirationDate = car.getFormExpirationDate();
        this.carType = car.getCarType();
        this.name = name;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        try {
            return Integer.parseInt(ownerId);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return Utils.checkString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return Utils.checkString(phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getIsActive() {
        return isActive == 1;
    }

    public void setIsActive(boolean isActive) {
        if (isActive)
            this.isActive = 1;
        else
            this.isActive = 0;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

//    public String getFormNumber() {
//        return formNumber;
//    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }

//    public String getFormExpirationDate() {
//        return formExpirationDate;
//    }

    public String getCarType() {
        return carType;
    }
}
