package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class City extends Area {

    @SerializedName("areas")
    private List<Area> areaList;

    public List<Area> getAreaList() {
        return areaList;
    }
}
