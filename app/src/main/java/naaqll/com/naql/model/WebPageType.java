package naaqll.com.naql.model;

import java.io.Serializable;

public enum WebPageType implements Serializable {
    ABOUT,TERMS_CONDITIONS
}
