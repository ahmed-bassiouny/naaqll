package naaqll.com.naql.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.helper.Constants;

public class CitiesAndRegions {

    private static Map<String, List<String>> citiesAndRegion;
    private static String[] citiesKSA, citiesAUE, Region;
    private static List<Country> countries;

    public static int ksaIndex = 0;
    public static int aueIndex = 1;

    private static boolean languageAr = true;


    public static void createCitiesAr() {
        citiesKSA = new String[countries.get(ksaIndex).getCityList().size()];
        citiesAUE = new String[countries.get(aueIndex).getCityList().size()];

        for (int i = 0; i < countries.get(ksaIndex).getCityList().size(); i++) {
            citiesKSA[i] = countries.get(ksaIndex).getCityList().get(i).getNameAr();
        }

        for (int i = 0; i < countries.get(aueIndex).getCityList().size(); i++) {
            citiesAUE[i] = countries.get(aueIndex).getCityList().get(i).getNameAr();
        }
    }

    public static void createCitiesEn() {
        citiesKSA = new String[countries.get(ksaIndex).getCityList().size()];
        citiesAUE = new String[countries.get(aueIndex).getCityList().size()];

        for (int i = 0; i < countries.get(ksaIndex).getCityList().size(); i++) {
            citiesKSA[i] = countries.get(ksaIndex).getCityList().get(i).getNameEn();
        }

        for (int i = 0; i < countries.get(aueIndex).getCityList().size(); i++) {
            citiesAUE[i] = countries.get(aueIndex).getCityList().get(i).getNameEn();
        }

    }

    public static int getCityId(int indexCounrty, int cityIndex) {
        return countries.get(indexCounrty).getCityList().get(cityIndex).getId();
    }

    public static int getRegionId(int indexCounrty, int cityIndex, int regionIndex) {
        return countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().get(regionIndex).getId();
    }


    public static String[] getRegion(int indexCounrty, int cityIndex) {
        if (languageAr)
            return getRegionAr(indexCounrty, cityIndex);
        else return getRegionEn(indexCounrty, cityIndex);
    }

    private static String[] getRegionAr(int indexCounrty, int cityIndex) {
        Region = new String[countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().size()];

        for (int i = 0; i < countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().size(); i++) {
            Region[i] = countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().get(i).getNameAr();
        }
        return Region;
    }

    private static String[] getRegionEn(int indexCounrty, int cityIndex) {
        Region = new String[countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().size()];

        for (int i = 0; i < countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().size(); i++) {
            Region[i] = countries.get(indexCounrty).getCityList().get(cityIndex).getAreaList().get(i).getNameEn();
        }
        return Region;
    }


    public static void setCountries(List<Country> countries) {
        CitiesAndRegions.countries = countries;
        User use = SharedPrefManager.getObject(Constants.USER, User.class);
        if (use != null)
            languageAr = use.getLang().equals(Constants.ARA);
        else
            languageAr = false;
        if (languageAr) {
            createCitiesAr();
        } else {
            createCitiesEn();
        }
    }


    public static String[] getCitiesKSA() {
        return citiesKSA;
    }

    public static List<City> getAllCity() {
        List<City> list = new ArrayList<>();
        list.addAll(countries.get(ksaIndex).getCityList());
        list.addAll(countries.get(aueIndex).getCityList());
        return list;
    }

    public static String[] getCitiesAUE() {
        return citiesAUE;
    }

    private static Map<String, List<String>> getCitiesAndRegion() {
        if (citiesAndRegion == null) {
            citiesAndRegion = new HashMap<>();
            // الرياض
            List<String> c1 = new ArrayList<>();
            c1.add("الرياض");
            c1.add("الدرعية");
            c1.add("الخرج");
            c1.add("الدوادمي");
            c1.add("المجمعة");
            c1.add("القويعية");
            c1.add("الأفلاج");
            c1.add("وادي الدواسر");
            c1.add("الزلفي");
            c1.add("شقراء");
            c1.add("حوطة بني تميم");
            c1.add("عفيف");
            c1.add("الغاط");
            c1.add("السليل");
            c1.add("ضرما");
            c1.add("المزاحمية");
            c1.add("رماح");
            c1.add("ثادق");
            c1.add("حريملاء");
            c1.add("الحريق");
            c1.add("مرات");
            c1.add("الرين");
            citiesAndRegion.put("الرياض", c1);


            // مكة المكرمة
            List<String> c2 = new ArrayList<>();
            c2.add("مكة المكرمة");
            c2.add("جدة");
            c2.add("الطائف");
            c2.add("القنفذة");
            c2.add("الليث");
            c2.add("رابغ");
            c2.add("خليص");
            c2.add("الخرمة");
            c2.add("رنية");
            c2.add("تربة");
            c2.add("الجموم");
            c2.add("الكامل");
            c2.add("المويه");
            c2.add("ميسان");
            c2.add("أضم");
            c2.add("العرضيات");
            c2.add("بحرة");
            citiesAndRegion.put("مكة المكرمة", c2);

            // المدينة المنورة
            List<String> c3 = new ArrayList<>();
            c3.add("المدينة المنورة");
            c3.add("ينبع");
            c3.add("العلا");
            c3.add("مهد الذهب");
            c3.add("الحناكية");
            c3.add("بدر");
            c3.add("خيبر");
            c3.add("العيص");
            c3.add("وادي الفرع");
            citiesAndRegion.put("المدينة المنورة", c3);


            // القصيم
            List<String> c4 = new ArrayList<>();
            c4.add("القصيم");
            c4.add("بريدة");
            c4.add("عنيزة");
            c4.add("الرس");
            c4.add("المذنب");
            c4.add("البكيرية");
            c4.add("البدائع");
            c4.add("الأسياح");
            c4.add("النبهانية");
            c4.add("الشماسية");
            c4.add("عيون الجواء");
            c4.add("رياض الخبراء");
            c4.add("عقلة الصقور");
            c4.add("ضرية");
            citiesAndRegion.put("القصيم", c4);


            // الشرقية
            List<String> c5 = new ArrayList<>();
            c5.add("المنطقة الشرقية");
            c5.add("الدمام");
            c5.add("الأحساء");
            c5.add("حفر الباطن");
            c5.add("الجبيل");
            c5.add("القطيف");
            c5.add("الخبر");
            c5.add("الخفجي");
            c5.add("رأس تنورة");
            c5.add("بقيق");
            c5.add("النعيرية");
            c5.add("قرية العليا");
            c5.add("العديد");
            citiesAndRegion.put("المنطقة الشرقية", c5);


            // عسير
            List<String> c6 = new ArrayList<>();
            c6.add("عسير");
            c6.add("أبها");
            c6.add("خميس مشيط");
            c6.add("بيشة");
            c6.add("النماص");
            c6.add("محايل عسير");
            c6.add("ظهران الجنوب");
            c6.add("تثليث");
            c6.add("سراة عبيدة");
            c6.add("رجال ألمع");
            c6.add("بلقرن");
            c6.add("أحد رفيدة");
            c6.add("المجاردة");
            c6.add("البرك");
            c6.add("بارق");
            c6.add("تنومة");
            c6.add("طريب");
            c6.add("الحرجة");
            citiesAndRegion.put("عسير", c6);

            // تبوك
            List<String> c7 = new ArrayList<>();
            c7.add("تبوك");
            c7.add("الوجه");
            c7.add("ضبا");
            c7.add("تيماء");
            c7.add("أملج");
            c7.add("حقل");
            c7.add("البدع");
            citiesAndRegion.put("تبوك", c7);

            // حائل
            List<String> c8 = new ArrayList<>();
            c8.add("حائل");
            c8.add("بقعاء");
            c8.add("الغزالة");
            c8.add("الشنان");
            c8.add("الحائط");
            c8.add("السليمي");
            c8.add("الشملي");
            c8.add("موقق");
            citiesAndRegion.put("حائل", c8);

            // الحدود الشمالية
            List<String> c9 = new ArrayList<>();
            c9.add("الحدود الشمالية");
            c9.add("عرعر");
            c9.add("رفحاء");
            c9.add("طريف");
            c9.add("العويقيلة");
            citiesAndRegion.put("الحدود الشمالية", c9);


            // جازان
            List<String> c10 = new ArrayList<>();
            c10.add("جازان");
            c10.add("صبيا");
            c10.add("أبو عريش");
            c10.add("صامطة");
            c10.add("بيش");
            c10.add("الدرب");
            c10.add("الحرث");
            c10.add("ضمد");
            c10.add("الريث");
            c10.add("جزر فرسان");
            c10.add("الدائر");
            c10.add("العارضة");
            c10.add("أحد المسارحة");
            c10.add("العيدابي");
            c10.add("فيفاء");
            c10.add("الطوال");
            c10.add("هروب");
            citiesAndRegion.put("جازان", c10);

            // نجران
            List<String> c11 = new ArrayList<>();
            c11.add("نجران");
            c11.add("شرورة");
            c11.add("حبونا");
            c11.add("بدر الجنوب");
            c11.add("يدمه");
            c11.add("ثار");
            c11.add("خباش");
            c11.add("الخرخير");
            citiesAndRegion.put("نجران", c11);

            // الباحة
            List<String> c12 = new ArrayList<>();
            c12.add("الباحة");
            c12.add("بلجرشي");
            c12.add("المندق");
            c12.add("المخواة");
            c12.add("قلوة");
            c12.add("العقيق");
            c12.add("القرى");
            c12.add("غامد الزناد");
            c12.add("الحجرة");
            c12.add("بني حسن");
            citiesAndRegion.put("الباحة", c12);

            // الجوف
            List<String> c13 = new ArrayList<>();
            c13.add("الجوف");
            c13.add("سكاكا");
            c13.add("دومة الجندل");
            c13.add("طبرجل");
            citiesAndRegion.put("الجوف", c13);


        }
        return citiesAndRegion;
    }

    /*public static List<String> getCities(){
        if(cities == null) {
            cities = new ArrayList<>();
            cities.add("الرياض");
            cities.add("مكة المكرمة");
            cities.add("المدينة المنورة");
            cities.add("المنطقة الشرقية");
            cities.add("الباحة");
            cities.add("القصيم");
            cities.add("جيزان");
            cities.add("عسير");
            cities.add("تبوك");
            cities.add("حائل");
            cities.add("الحدود الشمالية");
            cities.add("نجران");
            cities.add("الجوف");
            //cities.addAll(getCitiesAndRegion().keySet());
        }
        return cities;
    }*/

    public static List<String> getRegion(String city) {
        return getCitiesAndRegion().get(city);
    }
}
