package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

public class CompanyOrDriver {

    // data for company
    private String companyName;
    private String companyShipment;
    // data for driver
    private String driverName;
    private String driverPhone;
    @SerializedName("plate_number")
    private String plateNumber;
//    @SerializedName("form_number")
//    private String formNumber;
    @SerializedName("color")
    private String color;
    @SerializedName("model")
    private String model;
//    @SerializedName("form_end_date")
//    private String formExpirationDate;
    @SerializedName("car_type")
    private String carType;
    private boolean isCompany;

    public CompanyOrDriver(String companyName,int driversLenght) {
        this.companyName = companyName;
        this.companyShipment = String.valueOf(driversLenght);
        this.isCompany = true;
    }
    public CompanyOrDriver(Driver driver) {
        this.driverName = driver.getName();
        this.driverPhone = driver.getPhone();
        this.plateNumber = driver.getPlateNumber();
        //this.formNumber = driver.getFormNumber();
        this.color = driver.getColor();
        this.model = driver.getModel();
        //this.formExpirationDate = driver.getFormExpirationDate();
        this.carType = driver.getCarType();
        this.isCompany = false;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyShipment() {
        return companyShipment;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

//    public String getFormNumber() {
//        return formNumber;
//    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }

//    public String getFormExpirationDate() {
//        return formExpirationDate;
//    }

    public String getCarType() {
        return carType;
    }

    public boolean isCompany() {
        return isCompany;
    }
}
