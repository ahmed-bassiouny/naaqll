package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

public class Area {

    @SerializedName("id")
    private int Id;
    @SerializedName("name_ar")
    private String nameAr;
    @SerializedName("name_en")
    private String nameEn;

    public int getId() {
        return Id;
    }

    public String getNameAr() {
        return nameAr;
    }

    public String getNameEn() {
        return nameEn;
    }
}
