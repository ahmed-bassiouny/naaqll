package naaqll.com.naql.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Country extends Area{

    @SerializedName("cities")
    private List<City> cityList;

    public List<City> getCityList() {
        return cityList;
    }
}
