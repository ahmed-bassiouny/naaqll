package naaqll.com.naql.api;

import java.util.List;
import java.util.Map;

import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.base.api.BaseResponse;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.Country;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.DriversToTripRequest;
import naaqll.com.naql.model.PackageTrip;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ProjectApi {

    @POST("login")
    @FormUrlEncoded
    Call<BaseResponse<User>> login(@Field("email") String email, @Field("password") String password);

    @POST("forget_password")
    @FormUrlEncoded
    Call<BaseResponse> forgetPassword(@Field("email") String email);

    @POST("profile/edit")
    Call<BaseResponse<User>> editProfile(@Body User user);

    @POST("registration")
    Call<BaseResponse<User>> register(@Body User user);


    @POST("password/edit")
    @FormUrlEncoded
    Call<BaseResponse> editPassword(@Field("password") String Password, @Field("old_password") String oldPassword);

    @POST("password/new")
    @FormUrlEncoded
    Call<BaseResponse> resetPassword(@Field("password") String Password, @Field("reset_password_code") String code);

    @POST("compliant/create")
    Call<BaseResponse> addComplaint(@Body Compliant compliant);

    @POST("compliant/list")
    Call<BaseResponse<List<Compliant>>> getComplaints();

    @POST("page")
    @FormUrlEncoded
    Call<BaseResponse<String>> getAboutUs(@Field("name") String name);

    @POST("drivers/new/list")
    @FormUrlEncoded
    Call<BaseResponse<List<Driver>>> getDrivers(@Field("owner_id") int ownerId);


    @POST("add/new/driver")
    Call<BaseResponse<Driver>> createDriver(@Body Driver driver);

    @POST("delete/new/driver")
    @FormUrlEncoded
    Call<BaseResponse> deleteDriver(@Field("driver_id") int driverId);


    @POST("drivers/create")
    Call<BaseResponse> addDriversToTrip(@Body DriversToTripRequest request);


    @POST("update/new/driver")
    @FormUrlEncoded
    Call<BaseResponse<Driver>> editDriver(@Field("driver_id") int driverId,@Field("name") String name,@Field("phone") String phone);


    @POST("orders/create")
    Call<BaseResponse<Trip>> createTrip(@Body Trip trip);

    @POST("orders")
    @FormUrlEncoded
    Call<BaseResponse<BaseList<Trip>>> getTrips(@Field("offset") int offset, @Field("limit") int limit, @Field("status") String status);

    @POST("orders/create/status")
    @FormUrlEncoded
    Call<BaseResponse> changeStatus(@Field("order_id") int tripId,@Field("status") int status);

    @POST("packages")
    Call<BaseResponse<List<PackageTrip>>> getPackageTrip();

    @POST("orders/with/company/with/drivers")
    @FormUrlEncoded
    Call<BaseResponse<BaseList<Trip>>> getTripsHistory(@Field("offset") int offset, @Field("limit") int limit, @Field("status") String status);


    @POST("orders/still/open/orders")
    @FormUrlEncoded
    Call<BaseResponse<BaseList<Trip>>> getCurrentTrips(@Field("offset") int offset, @Field("limit") int limit, @Field("status") String status);

    @POST("points")
    Call<BaseResponse<String>> getPoint();

    @POST("countries")
    Call<BaseResponse<List<Country>>> getCountry();

    @POST("cars")
    @FormUrlEncoded
    Call<BaseResponse<List<Car>>> getCars(@Field("is_deleted") int isDeleted);

    @POST("cars/create")
    Call<BaseResponse<Car>> addCar(@Body Car car);

    @POST("cars/edit")
    Call<BaseResponse<Car>> editCar(@Body Car car);

    @POST
    @FormUrlEncoded
    Call<BaseResponse<BaseList<Trip>>> searchTrips(@Url String url, @Field("search_id") String tripId, @Field("from_date_created_at") String from,
                                                   @Field("to_date_created_at") String to, @Field("offset") int offset,
                                                   @Field("limit") int limit, @Field("status") String status);

}
