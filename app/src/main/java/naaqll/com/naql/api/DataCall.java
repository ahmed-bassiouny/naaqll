package naaqll.com.naql.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import naaqll.com.naql.base.api.BaseDataCall;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.base.api.BaseResponse;
import naaqll.com.naql.interactor.ICarInteractor;
import naaqll.com.naql.interactor.IComplaintInteractor;
import naaqll.com.naql.interactor.IDriverInteractor;
import naaqll.com.naql.interactor.IGeneralInteractor;
import naaqll.com.naql.interactor.ITripInteractor;
import naaqll.com.naql.interactor.IUserInteractor;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.Country;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.DriversToTripRequest;
import naaqll.com.naql.model.PackageTrip;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataCall extends BaseDataCall implements IUserInteractor, IComplaintInteractor, IGeneralInteractor,
        IDriverInteractor, ITripInteractor,ICarInteractor {

    @Override
    public void register(User user, final RequestCallback<User> callback) {
        Call<BaseResponse<User>> responseCall = RetrofitConfig.httpApiInterface.register(user);
        responseCall.enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void editProfile(User user, final RequestCallback<User> callback) {
        Call<BaseResponse<User>> responseCall = RetrofitConfig.httpApiInterface.editProfile(user);
        responseCall.enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void login(String email, String password, final RequestCallback<User> callback) {
        Call<BaseResponse<User>> responseCall = RetrofitConfig.httpApiInterface.login(email, password);
        responseCall.enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void forgetPassword(String email, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.forgetPassword(email);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });

    }

    @Override
    public void editPassword(String password, String oldPassword, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.editPassword(password, oldPassword);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getPoints(final RequestCallback<String> callback) {
        Call<BaseResponse<String>> responseCall = RetrofitConfig.httpApiInterface.getPoint();
        responseCall.enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void resetPassword(String password, String code, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.resetPassword(password, code);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void addComplaint(Compliant compliant, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.addComplaint(compliant);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getComplaints(final RequestCallback<List<Compliant>> callback) {
        Call<BaseResponse<List<Compliant>>> responseCall = RetrofitConfig.httpApiInterface.getComplaints();
        responseCall.enqueue(new Callback<BaseResponse<List<Compliant>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Compliant>>> call, Response<BaseResponse<List<Compliant>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Compliant>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void aboutUs(final RequestCallback<String> callback) {
        Call<BaseResponse<String>> responseCall = RetrofitConfig.httpApiInterface.getAboutUs("about");
        responseCall.enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getTermsConditions(final RequestCallback<String> callback) {
        Call<BaseResponse<String>> responseCall = RetrofitConfig.httpApiInterface.getAboutUs("terms_conditions");
        responseCall.enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getPackageTrip(final RequestCallback<List<PackageTrip>> callback) {
        Call<BaseResponse<List<PackageTrip>>> responseCall = RetrofitConfig.httpApiInterface.getPackageTrip();
        responseCall.enqueue(new Callback<BaseResponse<List<PackageTrip>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<PackageTrip>>> call, Response<BaseResponse<List<PackageTrip>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<PackageTrip>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getCountries(final RequestCallback<List<Country>> callback) {
        Call<BaseResponse<List<Country>>> responseCall = RetrofitConfig.httpApiInterface.getCountry();
        responseCall.enqueue(new Callback<BaseResponse<List<Country>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Country>>> call, Response<BaseResponse<List<Country>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Country>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void addDriver(Driver driver, final RequestCallback<Driver> callback) {
        Call<BaseResponse<Driver>> responseCall = RetrofitConfig.httpApiInterface.createDriver(driver);
        responseCall.enqueue(new Callback<BaseResponse<Driver>>() {
            @Override
            public void onResponse(Call<BaseResponse<Driver>> call, Response<BaseResponse<Driver>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<Driver>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void addDriverToTrip(int ownerId, int tripId, List<Driver> driverList, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.addDriversToTrip(new DriversToTripRequest(ownerId,tripId,driverList));
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void editDriver(Driver driver, final RequestCallback<Driver> callback) {
        Call<BaseResponse<Driver>> responseCall = RetrofitConfig.httpApiInterface.editDriver(driver.getId(),driver.getName(),driver.getPhone());
        responseCall.enqueue(new Callback<BaseResponse<Driver>>() {
            @Override
            public void onResponse(Call<BaseResponse<Driver>> call, Response<BaseResponse<Driver>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<Driver>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getDriver(int ownerId, final RequestCallback<List<Driver>> callback) {
        Call<BaseResponse<List<Driver>>> responseCall = RetrofitConfig.httpApiInterface.getDrivers(ownerId);
        responseCall.enqueue(new Callback<BaseResponse<List<Driver>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Driver>>> call, Response<BaseResponse<List<Driver>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Driver>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void deleteDriver(int driverId, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.deleteDriver(driverId);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void createTrip(Trip trip, final RequestCallback<Trip> callback) {
        Call<BaseResponse<Trip>> responseCall = RetrofitConfig.httpApiInterface.createTrip(trip);
        responseCall.enqueue(new Callback<BaseResponse<Trip>>() {
            @Override
            public void onResponse(Call<BaseResponse<Trip>> call, Response<BaseResponse<Trip>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<Trip>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void changeStatus(int tripId, int status, final RequestCallback callback) {
        Call<BaseResponse> responseCall = RetrofitConfig.httpApiInterface.changeStatus(tripId,status);
        responseCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                onDataResponseWithoutType(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void list(int offset,int limit,final String status, final RequestCallback<BaseList<Trip>> callback) {
        Call<BaseResponse<BaseList<Trip>>> responseCall = RetrofitConfig.httpApiInterface.getTrips(offset,limit,status);
        responseCall.enqueue(new Callback<BaseResponse<BaseList<Trip>>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseList<Trip>>> call, Response<BaseResponse<BaseList<Trip>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseList<Trip>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getTripsHistory(int offset, int limit, String status, final RequestCallback<BaseList<Trip>> callback) {
        Call<BaseResponse<BaseList<Trip>>> responseCall = RetrofitConfig.httpApiInterface.getTripsHistory(offset,limit,status);
        responseCall.enqueue(new Callback<BaseResponse<BaseList<Trip>>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseList<Trip>>> call, Response<BaseResponse<BaseList<Trip>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseList<Trip>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getCurrentTrips(int offset, int limit, String status, final RequestCallback<BaseList<Trip>> callback) {
        Call<BaseResponse<BaseList<Trip>>> responseCall = RetrofitConfig.httpApiInterface.getCurrentTrips(offset,limit,status);
        responseCall.enqueue(new Callback<BaseResponse<BaseList<Trip>>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseList<Trip>>> call, Response<BaseResponse<BaseList<Trip>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseList<Trip>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void searchTrip(String url,String tripId, String dateFrom, String dateTo, final RequestCallback<BaseList<Trip>> callback) {
        Call<BaseResponse<BaseList<Trip>>> responseCall = RetrofitConfig.httpApiInterface.searchTrips(url,tripId,dateFrom,dateTo
        ,0,100,"0,1");
        responseCall.enqueue(new Callback<BaseResponse<BaseList<Trip>>>() {
            @Override
            public void onResponse(Call<BaseResponse<BaseList<Trip>>> call, Response<BaseResponse<BaseList<Trip>>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<BaseList<Trip>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }


    @Override
    public void addCar(Car car, final RequestCallback<Car> callback) {
        Call<BaseResponse<Car>> responseCall = RetrofitConfig.httpApiInterface.addCar(car);
        responseCall.enqueue(new Callback<BaseResponse<Car>>() {
            @Override
            public void onResponse(Call<BaseResponse<Car>> call, Response<BaseResponse<Car>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<Car>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void editCar(Car car, final RequestCallback<Car> callback) {
        Call<BaseResponse<Car>> responseCall = RetrofitConfig.httpApiInterface.editCar(car);
        responseCall.enqueue(new Callback<BaseResponse<Car>>() {
            @Override
            public void onResponse(Call<BaseResponse<Car>> call, Response<BaseResponse<Car>> response) {
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<Car>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }

    @Override
    public void getCars(final RequestCallback<List<Car>> callback) {
        Call<BaseResponse<List<Car>>> responseCall = RetrofitConfig.httpApiInterface.getCars(0);
        responseCall.enqueue(new Callback<BaseResponse<List<Car>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Car>>> call, Response<BaseResponse<List<Car>>> response) {
                onDataResponse(response, callback);
                onDataResponse(response, callback);
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Car>>> call, Throwable t) {
                onDataFailure(t, callback);
            }
        });
    }
}
