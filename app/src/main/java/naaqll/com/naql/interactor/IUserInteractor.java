package naaqll.com.naql.interactor;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.User;

public interface IUserInteractor {

    void register(User user, RequestCallback<User> callback);
    void editProfile(User user, RequestCallback<User> callback);
    void login(String email, String password, RequestCallback<User> callback);
    void forgetPassword(String email, RequestCallback callback);
    void editPassword(String password,String oldPassword, RequestCallback callback);
    void getPoints(RequestCallback<String> callback);
    void resetPassword(String password,String code,RequestCallback callback);
}
