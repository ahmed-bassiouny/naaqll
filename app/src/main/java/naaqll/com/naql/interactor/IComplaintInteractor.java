package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Compliant;
import naaqll.com.naql.model.User;

public interface IComplaintInteractor {
    void addComplaint(Compliant compliant, RequestCallback callback);
    void getComplaints(RequestCallback<List<Compliant>> callback);
}
