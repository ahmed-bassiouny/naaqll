package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Compliant;

public class ComplaintInteractor implements IComplaintInteractor {

    IComplaintInteractor interactor;

    public ComplaintInteractor() {
        this.interactor = new DataCall();
    }

    @Override
    public void addComplaint(Compliant compliant, RequestCallback callback) {
        interactor.addComplaint(compliant,callback);
    }

    @Override
    public void getComplaints(RequestCallback<List<Compliant>> callback) {
        interactor.getComplaints(callback);
    }
}
