package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Car;

public interface ICarInteractor {

    void addCar(Car car , RequestCallback<Car> callback);
    void editCar(Car car , RequestCallback<Car> callback);
    void getCars(RequestCallback<List<Car>> callback);
}
