package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Driver;

public class DriverInteractor implements IDriverInteractor {

    private IDriverInteractor interactor;

    public DriverInteractor() {
        interactor = new DataCall();
    }

    @Override
    public void addDriver(Driver driver, RequestCallback callback) {
        interactor.addDriver(driver,callback);
    }

    @Override
    public void editDriver(Driver driver, RequestCallback callback) {
        interactor.editDriver(driver,callback);
    }

    @Override
    public void addDriverToTrip(int ownerId, int tripId, List<Driver> driverList, RequestCallback callback) {
        interactor.addDriverToTrip(ownerId, tripId, driverList, callback);
    }

    @Override
    public void getDriver(int ownerId, RequestCallback<List<Driver>> callback) {
        interactor.getDriver(ownerId,callback);
    }

    @Override
    public void deleteDriver(int driverId, RequestCallback callback) {
        interactor.deleteDriver(driverId,callback);
    }
}
