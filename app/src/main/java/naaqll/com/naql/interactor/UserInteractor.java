package naaqll.com.naql.interactor;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.User;

public class UserInteractor implements IUserInteractor {

    private IUserInteractor interactor;

    public UserInteractor() {
        this.interactor = new DataCall();
    }

    @Override
    public void register(User user, RequestCallback<User> callback) {
        user.setName("name");
        interactor.register(user, callback);
    }

    @Override
    public void editProfile(User user, RequestCallback<User> callback) {
        interactor.editProfile(user, callback);
    }

    @Override
    public void login(String email, String password, RequestCallback<User> callback) {
        interactor.login(email, password, callback);
    }

    @Override
    public void forgetPassword(String email, RequestCallback callback) {
        interactor.forgetPassword(email, callback);
    }

    @Override
    public void editPassword(String password, String oldPassword, RequestCallback callback) {
        interactor.editPassword(password, oldPassword, callback);
    }

    @Override
    public void getPoints(RequestCallback<String> callback) {
        interactor.getPoints(callback);
    }

    @Override
    public void resetPassword(String password, String code, RequestCallback callback) {
        interactor.resetPassword(password, code, callback);
    }
}
