package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Country;
import naaqll.com.naql.model.PackageTrip;

public interface IGeneralInteractor {
    void aboutUs(RequestCallback<String> callback);
    void getTermsConditions(RequestCallback<String> callback);
    void getPackageTrip(RequestCallback<List<PackageTrip>> callback);
    void getCountries(RequestCallback<List<Country>> callback);
}
