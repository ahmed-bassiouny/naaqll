package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Driver;

public interface IDriverInteractor {

    void addDriver(Driver driver , RequestCallback<Driver> callback);
    void addDriverToTrip(int ownerId,int tripId,List<Driver> driverList, RequestCallback callback);
    void editDriver(Driver driver , RequestCallback<Driver> callback);
    void getDriver(int ownerId, RequestCallback<List<Driver>> callback);
    void deleteDriver(int driverId, RequestCallback callback);

}
