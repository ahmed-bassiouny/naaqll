package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Country;
import naaqll.com.naql.model.PackageTrip;

public class GeneralInteractor implements IGeneralInteractor {

    private IGeneralInteractor interactor;

    public GeneralInteractor() {
        interactor = new DataCall();
    }

    @Override
    public void aboutUs(RequestCallback<String> callback) {
        interactor.aboutUs(callback);
    }

    @Override
    public void getTermsConditions(RequestCallback<String> callback) {
        interactor.getTermsConditions(callback);
    }

    @Override
    public void getPackageTrip(RequestCallback<List<PackageTrip>> callback) {
        interactor.getPackageTrip(callback);
    }

    @Override
    public void getCountries(RequestCallback<List<Country>> callback) {
        interactor.getCountries(callback);

    }
}
