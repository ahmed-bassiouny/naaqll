package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.model.Trip;

public class TripInteractor implements ITripInteractor {

    private ITripInteractor interactor;

    public TripInteractor() {
        interactor = new DataCall();
    }

    @Override
    public void createTrip(Trip trip, RequestCallback<Trip> callback) {
        interactor.createTrip(trip,callback);
    }

    @Override
    public void changeStatus(int tripId,int status, RequestCallback callback) {
        interactor.changeStatus(tripId,status,callback);
    }

    @Override
    public void list(int offset,int limit,String status,RequestCallback<BaseList<Trip>> callback) {
        interactor.list(offset,limit,status,callback);

    }

    @Override
    public void getTripsHistory(int offset, int limit, String status, RequestCallback<BaseList<Trip>> callback) {
        interactor.getTripsHistory(offset, limit, status, callback);
    }

    @Override
    public void getCurrentTrips(int offset, int limit, String status, RequestCallback<BaseList<Trip>> callback) {
        interactor.getCurrentTrips(offset, limit, status, callback);
    }

    @Override
    public void searchTrip(String url,String tripId, String dateFrom, String dateTo, RequestCallback<BaseList<Trip>> callback) {
        interactor.searchTrip(url,tripId, dateFrom, dateTo, callback);
    }
}
