package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.DataCall;
import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Driver;

public class CarInteractor implements ICarInteractor {

    private ICarInteractor interactor;

    public CarInteractor() {
        interactor = new DataCall();
    }


    @Override
    public void addCar(Car car, RequestCallback<Car> callback) {
        interactor.addCar(car, callback);
    }

    @Override
    public void editCar(Car car, RequestCallback<Car> callback) {
        interactor.editCar(car, callback);
    }

    @Override
    public void getCars(RequestCallback<List<Car>> callback) {
        interactor.getCars(callback);
    }
}
