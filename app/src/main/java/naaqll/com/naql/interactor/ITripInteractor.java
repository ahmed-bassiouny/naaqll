package naaqll.com.naql.interactor;

import java.util.List;

import naaqll.com.naql.api.RequestCallback;
import naaqll.com.naql.base.api.BaseList;
import naaqll.com.naql.model.Trip;

public interface ITripInteractor {

    void createTrip(Trip trip ,RequestCallback<Trip> callback);
    void changeStatus(int tripId,int status,RequestCallback callback);
    void list(int offset,int limit,String status,RequestCallback<BaseList<Trip>> callback);
    void getTripsHistory(int offset,int limit,String status,RequestCallback<BaseList<Trip>> callback);
    void getCurrentTrips(int offset,int limit,String status,RequestCallback<BaseList<Trip>> callback);
    void searchTrip(String url,String tripId,String dateFrom,String dateTo,RequestCallback<BaseList<Trip>> callback);

}
