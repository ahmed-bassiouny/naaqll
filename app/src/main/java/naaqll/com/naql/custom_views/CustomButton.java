package naaqll.com.naql.custom_views;

import android.content.Context;
import android.util.AttributeSet;


import naaqll.com.naql.R;

public class CustomButton extends android.support.v7.widget.AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        this.setBackground(getResources().getDrawable(R.drawable.border_red_fill));
        this.setTextColor(getResources().getColor(R.color.white));
    }
}
