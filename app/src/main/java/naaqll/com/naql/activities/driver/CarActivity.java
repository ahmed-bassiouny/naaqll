package naaqll.com.naql.activities.driver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.fragments.driver.ViewCarsFragment;

public class CarActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);

        setTitle(getString(R.string.cars));
        MyFragmentTransaction.open(this,new ViewCarsFragment(),R.id.main_frame,null);
    }
}
