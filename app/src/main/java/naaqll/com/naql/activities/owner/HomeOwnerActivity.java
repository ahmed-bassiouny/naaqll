package naaqll.com.naql.activities.owner;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.driver.HomeDriverActivity;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.fragments.AboutUsFragment;
import naaqll.com.naql.fragments.ComplaintsFragment;
import naaqll.com.naql.fragments.MyProfileFragment;
import naaqll.com.naql.fragments.OptionsFragment;
import naaqll.com.naql.fragments.owner.OwnerOrdersFragment;


public class HomeOwnerActivity extends BaseActivity {

    @BindView(R.id.navigationView)
    BottomNavigationView navigationView;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    @BindView(R.id.view_page)
    ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_owner);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        navigationView.setOnNavigationItemSelectedListener(listener);
        navigationView.setSelectedItemId(R.id.orders);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);



        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                navigationView.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener listener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.more:
                    setTitle(getString(R.string.more));
                    mViewPager.setCurrentItem(3);
                    break;
                case R.id.profile:
                    setTitle(getString(R.string.profile));
                    mViewPager.setCurrentItem(2);
                    break;
                case R.id.complaines:
                    setTitle(getString(R.string.complaines));
                    mViewPager.setCurrentItem(1);
                    break;
                case R.id.orders:
                    setTitle(getString(R.string.orders));
                    mViewPager.setCurrentItem(0);
                    break;
            }
            return true;
        }
    };


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return OwnerOrdersFragment.newInstance();
                case 1:
                    return ComplaintsFragment.newInstance();
                case 2:
                    return MyProfileFragment.newInstance();
                case 3:
                    return OptionsFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
           return 4;
        }
    }
}
