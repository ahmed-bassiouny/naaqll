package naaqll.com.naql.activities.driver;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.DriverAdapter;
import naaqll.com.naql.adapter.IDriverAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.DriversController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.Driver;

public class DriverListActivity extends BaseActivity implements BaseController.IResult<List<Driver>>, IDriverAdapter {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.tv_no_driver)
    TextView tvNoDriver;

    private DriversController controller;
    private DriverAdapter adapter;
    private int requestCode = 123;
    private int lastPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_list);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.drivers));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DriverAdapter(this, this);
        recyclerView.setAdapter(adapter);
        controller = new DriversController(this);
        controller.getDriver(this);
    }

    @OnClick(R.id.btn_add_driver)
    void onBtnAddDriverClick() {
        lastPosition = -1;
        controller.launchActivityForResult(AddDriverActivity.class, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK && requestCode == this.requestCode && data != null) {
            Driver item = (Driver) data.getSerializableExtra(Constants.DRIVER);
                // check last item user click on
                if (lastPosition == -1) {
                    // if position -1 this mean user add driver
                    adapter.add(item);
                } else {
                    // if position > 1 this mean user edit or delete driver
                    if (item == null) {
                        // deleted object
                        adapter.delete(lastPosition);
                    }else {
                        // updated object
                        adapter.update(lastPosition, item);
                    }
                }

        } else {
            // redefine last position
            lastPosition = -1;
        }
    }

    @Override
    public void result(List<Driver> drivers) {
        if (drivers == null || drivers.size() == 0) {
            tvNoDriver.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            adapter.addList(drivers);
            tvNoDriver.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void editDriver(int position, Driver driver) {
        lastPosition = position;
        Intent intent = new Intent(this, AddDriverActivity.class);
        intent.putExtra(Constants.DRIVER, driver);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void changeStatusDriver(int position, Driver driver) {

    }
}
