package naaqll.com.naql.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;

public class CancelTripActivity extends AppCompatActivity {

    private int tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_trip);
        tripId = getIntent().getIntExtra("data", 0);
        if (tripId == 0) {
            finish();
            return;
        }
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_cancel)
    public void cancelTrip() {

    }
}
