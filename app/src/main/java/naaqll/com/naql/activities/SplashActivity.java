package naaqll.com.naql.activities;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.controller.SplashController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.LocalApp;
import naaqll.com.naql.model.User;


public class SplashActivity extends BaseActivity {

    private final int SPLASH_TIME_OUT = 500;
    private SplashController controller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash);
        controller = new SplashController(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               controller.getCountries();
            }
        }, SPLASH_TIME_OUT);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.ads).setVisibility(View.VISIBLE);
                findViewById(R.id.ads).startAnimation(animation());
            }
        }, 1000);*/
    }

    private Animation animation(){
        Animation animation= new TranslateAnimation(Animation.RELATIVE_TO_PARENT,+1.0f
                ,Animation.RELATIVE_TO_PARENT,+0
                ,Animation.RELATIVE_TO_PARENT,+0
                ,Animation.RELATIVE_TO_PARENT,+0);
        animation.setDuration(500);
        animation.setInterpolator(new AccelerateInterpolator());
        return animation;
    }
}
