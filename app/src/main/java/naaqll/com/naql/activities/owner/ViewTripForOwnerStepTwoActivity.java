package naaqll.com.naql.activities.owner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.CompanyOrDriverAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.CompanyOrDriver;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.DriversCompany;
import naaqll.com.naql.model.Trip;

public class ViewTripForOwnerStepTwoActivity extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.tv_trip_id)
    TextView tvTripId;
    @BindView(R.id.tv_no_company)
    TextView tvNoCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trip_for_owner_step_two);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.upcoming_driver));
        ButterKnife.bind(this);
        Trip trip = (Trip) getIntent().getSerializableExtra("data");
        tvTripId.setText(String.format(Locale.getDefault(),"%s : %d",getString(R.string.order_number),(Constants.TRIP_ID_START_FROM+trip.getId())));

        if(trip.getDriversCompanies().size() == 0 ){
            tvNoCompany.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }else {
            tvNoCompany.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        List<CompanyOrDriver> list = new ArrayList<>();
        for(DriversCompany item : trip.getDriversCompanies()){
            list.add(new CompanyOrDriver(item.getCompanyName(),item.getDriver().size()));
            for(Driver driver:item.getDriver()){
                list.add(new CompanyOrDriver(driver));
            }
        }
        recyclerView.setAdapter(new CompanyOrDriverAdapter(this,list));
        recyclerView.scrollToPosition(0);
    }
}
