package naaqll.com.naql.activities.driver;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.NotificationForDriverAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.CitiesAndRegions;
import naaqll.com.naql.model.City;
import naaqll.com.naql.model.Notification;
import naaqll.com.naql.model.User;

public class NotificationSettingActivity extends BaseActivity {


    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    private NotificationForDriverAdapter adapter;
    private List<Notification> lists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_setting);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.notification));
        lists = new ArrayList<>();
        List<City> arr = CitiesAndRegions.getAllCity();
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        for (City item : arr) {
            if (user.getLang().equals(Constants.ARA))
                lists.add(new Notification(item.getId(), item.getNameAr()));
            else
                lists.add(new Notification(item.getId(), item.getNameEn()));
        }
        adapter = new NotificationForDriverAdapter(lists);
        getData();
        recycler.setAdapter(adapter);
    }

    private void getData() {
        String ids = SharedPrefManager.getString(Constants.NOTIFICATION);
        if (ids.isEmpty())
            return;
        List<City> arr = CitiesAndRegions.getAllCity();
        String[] userSelectedId = ids.split(",");
        for(int i=0;i<arr.size();i++){
            for(String item:userSelectedId){
                if(arr.get(i).getId() == Integer.valueOf(item))
                    adapter.updateFiled(i);
            }
        }

    }

    @OnClick(R.id.btn_save)
    public void save() {
        unsubscribe();
        String ids = "";
        int size = adapter.getItemCount();
        lists = adapter.getList();
        for (int i = 0; i < size; i++) {
            if (lists.get(i).isSelected()) {
                ids = ids + lists.get(i).getId() + ",";
                FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf(i));
            }
        }
        // remove last element
        ids = ids.substring(0, ids.length() - 1);
        SharedPrefManager.setString(Constants.NOTIFICATION, ids);
        onBackPressed();
    }

    public void unsubscribe() {
        String ids = SharedPrefManager.getString(Constants.NOTIFICATION);
        if (ids.isEmpty())
            return;
        String[] arr = ids.split(",");
        for (String item : arr) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(String.valueOf(item));

        }
    }

}
