package naaqll.com.naql.activities.driver;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.activities.SplashActivity;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.CarController;
import naaqll.com.naql.controller.DriverController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Driver;
import naaqll.com.naql.model.User;

public class AddDriverActivity extends BaseActivity implements BaseController.IResult<Driver> {

    @BindView(R.id.et_driver_name)
    TextInputEditText etDriverName;
    @BindView(R.id.et_phone)
    TextInputEditText etPhone;
    @BindView(R.id.btn_delete)
    Button btnDelete;

    private DriverController controller;

    private Driver driver;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_driver);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.add_driver));
        controller = new DriverController(this, this);

        driver = (Driver) getIntent().getSerializableExtra(Constants.DRIVER);
        setDataUI();
        user = SharedPrefManager.getObject(Constants.USER,User.class);
    }

    private void setDataUI() {
        if (driver == null) {
            driver = new Driver();
            btnDelete.setVisibility(View.GONE);
        }else {
            etDriverName.setText(driver.getName());
            etPhone.setText(driver.getPhone());
        }
    }

    @OnClick(R.id.btn_save)
    void onBtnSaveClick() {
        boolean check = true;
        if (checkFiled(etDriverName)) {
            check = false;
        }
        if (checkFiled(etPhone)) {
            check = false;
        }
        if (check) {
            driver.setOwnerId(String.valueOf(user.getId()));
            driver.setName(etDriverName.getText().toString());
            driver.setPhone(etPhone.getText().toString());
            driver.setIsActive(true);
            if (driver.getId() > 0) {
                driver.setDriverId(driver.getId());
                controller.editDriver(driver);
            }
            else controller.addDriver(driver);
        }
    }

    @OnClick(R.id.btn_delete)
    void onBtnDelete(){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.are_you_sure));
            builder.setCancelable(true);

            builder.setPositiveButton(
                    getString(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            controller.deleteDriver(driver.getId(), new BaseController.IResult() {
                                @Override
                                public void result(Object o) {
                                    Intent data = new Intent();
                                    setResult(RESULT_OK, data);
                                    finish();
                                }
                            });
                        }
                    });

            builder.setNegativeButton(getString(R.string.no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
    }

    private boolean checkFiled(TextInputEditText input) {
        if (input.getText().toString().trim().isEmpty()) {
            input.setError(getString(R.string.required));
            return true;
        }
        return false;
    }

    @Override
    public void result(Driver driver) {
        Intent data = new Intent();
        data.putExtra(Constants.DRIVER, driver);
        setResult(RESULT_OK, data);
        finish();
    }
}
