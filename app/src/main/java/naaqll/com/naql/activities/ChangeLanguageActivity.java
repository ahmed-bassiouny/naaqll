package naaqll.com.naql.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.controller.AuthController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.LocalApp;
import naaqll.com.naql.helper.MyApplication;
import naaqll.com.naql.model.User;

public class ChangeLanguageActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.linear_english)
    public void setEnglishLang() {
        User user = SharedPrefManager.getObject(Constants.USER,User.class);
        if(user != null && user.getLang().equals(Constants.ARA)){
            user.setLang(Constants.ENG);
            LocalApp.init(this,user.getLang());
            SharedPrefManager.setObject(Constants.USER, user);

            Intent refresh = new Intent(this, SplashActivity.class);
            refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(refresh);
            finish();
        }else {
            finish();
        }
    }

    @OnClick(R.id.linear_arabic)
    public void setArabicLang() {
        User user = SharedPrefManager.getObject(Constants.USER,User.class);
        if(user != null && user.getLang().equals(Constants.ENG)){
            user.setLang(Constants.ARA);
            LocalApp.init(this,user.getLang());


            SharedPrefManager.setObject(Constants.USER, user);
            Intent refresh = new Intent(this, SplashActivity.class);
            refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(refresh);
            finish();
        }else {
            finish();
        }
    }


}
