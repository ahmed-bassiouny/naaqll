package naaqll.com.naql.activities.owner;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.TripDetailsAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.TripDetailsController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Utils;
import naaqll.com.naql.model.DriversCompany;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;

public class ViewTripForOwnerActivity extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.tv_trip_status)
    TextView tvTripStatus;
    @BindView(R.id.btn_cancel)
    Button btnCancel;

    private Trip trip;
    private TripDetailsController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trip_for_owner);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.trip_details));
        trip = (Trip) getIntent().getSerializableExtra("data");
        if (trip == null) {
            finish();
            return;
        }
        tvTripStatus.setText(getString(trip.getStatusString()));
        tvTripStatus.setBackgroundColor(getResources().getColor(trip.getStatusColor()));
        String[] arrName = {
                getString(R.string.order_number)
                , getString(R.string.company_name)
                , getString(R.string.from_city)
                , getString(R.string.from_address)
                , getString(R.string.to_city)
                , getString(R.string.to_address)
                , getString(R.string.products_type)
                , getString(R.string.request_type)
                , getString(R.string.shipments_required)
                , getString(R.string.shipments)
                , getString(R.string.load)
                , getString(R.string.price)
                , getString(R.string.created_at)
                , getString(R.string.from_time)
                , getString(R.string.to_time)
                , getString(R.string.holiday)
                , getString(R.string.return_city)
                , getString(R.string.return_location)
                , getString(R.string.saving_cost)
                , getString(R.string.payment_method)
        };

        String[] arrValue = {
                String.valueOf(trip.getId() + Constants.TRIP_ID_START_FROM)
                , trip.getCompanyName()
                , trip.getFromCity()
                , trip.getFromAddress()
                , trip.getToCity()
                , trip.getToAddress()
                , getResources().getStringArray(R.array.products_type)[trip.getOrderType()]
                , Utils.getStringFromArrayByIndex(this, R.array.trip_type, trip.getTripType())
                , trip.getNumberOfShipmentsAvailable()
                , String.valueOf(trip.getNumberOfShipments() - trip.getDriversCount())
                , trip.getLoadInTon()
                , trip.getPrice()
                , trip.getDateTrip()
                , trip.getFromTime()
                , trip.getToTime()
                , trip.getHoliday()
                , trip.getReturnCity()
                , trip.getReturnAddress()
                , Utils.getStringFromArrayByIndex(this, R.array.saving_cost, trip.getSaveCost())
                , getResources().getStringArray(R.array.payment_method)[trip.getPaymentMethod()]
        };
        recyclerView.setAdapter(new TripDetailsAdapter(this, arrName, arrValue));
        handleButton();
        controller = new TripDetailsController(this);

    }

   /* private String checkTime(String time){
        if (time.trim().isEmpty()){
            return getString(R.string.available_hours);
        }else {
            return time;
        }
    }*/

    private void handleButton() {
        if (trip.getStatus() == Trip.RUNNING)
            btnCancel.setText(getString(R.string.cancel_request));
        else btnCancel.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.btn_cancel)
    public void cancel() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(getString(R.string.are_you_sure));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        controller.makeTripUnavailable(trip.getId(), new BaseController.IResult<Boolean>() {
                            @Override
                            public void result(Boolean aBoolean) {
                                if (aBoolean) {
                                    btnCancel.setVisibility(View.INVISIBLE);
                                    Intent data = new Intent();
                                    setResult(RESULT_OK, data);
                                    finish();
                                }
                            }
                        });
                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    @OnClick(R.id.tv_details)
    public void details() {
        Intent intent = new Intent(this, ViewTripForOwnerStepTwoActivity.class);
        intent.putExtra("data", trip);
        startActivity(intent);
    }

    @OnClick(R.id.btnPrint)
    public void print() {
        String direction = "left";
        String lay = "ltr";
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        if (user != null && user.getLang().equals(Constants.ARA)) {
            direction = "right";
            lay = "rtl";
        }

        String header = "<html dir="+lay+">" +
                "<head>" +
                "<style>" +
                "table {" +
                "  border-collapse: collapse;" +
                "  width: 100%;" +
                "}" +
                "" +
                "td, th {" +
                "  border: 1px solid #dddddd;" +
                "  text-align: "+direction+";" +
                "  padding: 8px;" +
                "}" +
                "" +
                "</style>" +
                "</head>" +
                "<body>";
        String body = "<table>" +
                "  <tr>" +
                "    <th>%s</th>" +
                "    <th>%s</th>" +
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + // 1
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //2
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //3
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //4
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //5
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //6
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //7
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //8
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //9
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //10
                "  </tr>" +
                "  <tr>" +
                "    <td>%s</td>" +
                "    <td>%s</td>" + //11
                "  </tr>" +
                "</table>" +
                "" +
                "</body>" +
                "</html>";

        String newContent = String.format(body,
                getString(R.string.order_number), trip.getId(),
                getString(R.string.company_name), trip.getCompanyName(), //1
                getString(R.string.from_city), trip.getFromCity(), //2
                getString(R.string.from_address), trip.getFromAddress(), //3
                getString(R.string.to_city), trip.getToCity(), //4
                getString(R.string.to_address), trip.getToAddress(), //5
                getString(R.string.products_type), getResources().getStringArray(R.array.products_type)[trip.getOrderType()], //6
                getString(R.string.request_type), Utils.getStringFromArrayByIndex(this, R.array.trip_type, trip.getTripType()), //7
                getString(R.string.shipments_required), trip.getNumberOfShipmentsAvailable(), //8
                getString(R.string.shipments), String.valueOf(trip.getNumberOfShipments() - trip.getDriversCount()), //9
                getString(R.string.load), trip.getLoadInTon(), //10
                getString(R.string.price), trip.getPrice() //11
        );
        WebView webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", header + newContent, "text/html", "UTF-8", "");

        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            printAdapter = webView.createPrintDocumentAdapter("my.pdf");
        } else {
            printAdapter = webView.createPrintDocumentAdapter();
        }

        // Create a print job with name and adapter instance
        String jobName = getString(R.string.app_name);

        PrintAttributes attributes = new PrintAttributes.Builder()
                /* .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                 .setResolution(new PrintAttributes.Resolution("id", Context.PRINT_SERVICE, 200, 200))
                 .setColorMode(PrintAttributes.COLOR_MODE_COLOR)
                 .setMinMargins(PrintAttributes.Margins.NO_MARGINS)*/
                .build();

        printManager.print(jobName, printAdapter, attributes);

    }

}
