package naaqll.com.naql.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.fragments.AboutUsFragment;

public class WebPageActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_page);
        initToolbar(R.id.toolbar);
        boolean about = getIntent().getBooleanExtra("about",false);
        if(about){
            setTitle(getString(R.string.about_us));
            MyFragmentTransaction.open(this, AboutUsFragment.newInstanceAbout(), R.id.main_frame, null);
        }else {
            setTitle(getString(R.string.terms_condition));
            MyFragmentTransaction.open(this, AboutUsFragment.newInstanceTerms(), R.id.main_frame, "terms");
        }
    }
}
