package naaqll.com.naql.activities.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.TripDetailsAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Utils;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;

public class ViewTripForDriverActivity extends BaseActivity {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.tv_trip_status)
    TextView tvTripStatus;
    @BindView(R.id.tv_details)
    TextView tvDetails;
    @BindView(R.id.btn_send_drivers)
    Button btnSendDrivers;

    private Trip trip;
    private boolean showSendDrivers = false;
    private TripDetailsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_trip_for_driver);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.trip_details));
        trip = (Trip) getIntent().getSerializableExtra("data");
        showSendDrivers = getIntent().getBooleanExtra("show", false);
        if (showSendDrivers) {
            btnSendDrivers.setVisibility(View.VISIBLE);
            tvDetails.setVisibility(View.GONE);
        } else {
            btnSendDrivers.setVisibility(View.GONE);
            tvDetails.setVisibility(View.VISIBLE);
        }
        if (trip == null) {
            finish();
            return;
        }
        if (trip.getNumberOfShipments() == trip.getDriversCount() || trip.getNumberOfShipments() == trip.getDriver().size()) {
            // complete drivers
            tvTripStatus.setText(getString(R.string.inactive));
            tvTripStatus.setBackgroundColor(getResources().getColor(R.color.red));
        }else {
            // uncomplete drivers
            tvTripStatus.setText(getString(R.string.active));
            tvTripStatus.setBackgroundColor(getResources().getColor(R.color.green));
        }
        String[] arrName = {
                getString(R.string.bill_number)
                , getString(R.string.company_name)
                , getString(R.string.from_city)
                , getString(R.string.from_address)
                , getString(R.string.to_city)
                , getString(R.string.to_address)
                , getString(R.string.products_type)
                , getString(R.string.request_type)
                , getString(R.string.shipments_required)
                , getString(R.string.shipments)
                , getString(R.string.load)
                , getString(R.string.price)
                , getString(R.string.created_at)
                , getString(R.string.from_time)
                , getString(R.string.to_time)
                , getString(R.string.holiday)
                , getString(R.string.return_city)
                , getString(R.string.return_location)
                , getString(R.string.saving_cost)
                ,getString(R.string.payment_method)
                , getString(R.string.drivers_count)
                // last item must be driver count
        };
        // get user
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        String billNumber = user.getId() + "" + trip.getId();
        String[] arrValue = {
                billNumber
                , trip.getCompanyName()
                , trip.getFromCity()
                , trip.getFromAddress()
                , trip.getToCity()
                , trip.getToAddress()
                , getResources().getStringArray(R.array.products_type)[trip.getOrderType()]
                , Utils.getStringFromArrayByIndex(this, R.array.trip_type, trip.getTripType())
                , trip.getNumberOfShipmentsAvailable()
                , String.valueOf(trip.getNumberOfShipments() - trip.getDriversCount())
                , trip.getLoadInTon()
                , trip.getPrice()
                , trip.getDateTrip()
                , trip.getFromTime()
                , trip.getToTime()
                , trip.getHoliday()
                , trip.getReturnCity()
                , trip.getReturnAddress()
                , Utils.getStringFromArrayByIndex(this, R.array.saving_cost, trip.getSaveCost())
                ,getResources().getStringArray(R.array.payment_method)[trip.getPaymentMethod()]
                , String.valueOf(trip.getDriversCount())
                // last item must be driver count
        };
        adapter = new TripDetailsAdapter(this, arrName, arrValue);
        recyclerView.setAdapter(adapter);
    }

   /* private String checkTime(String time){
        if (time.trim().isEmpty()){
            return getString(R.string.available_hours);
        }else {
            return time;
        }
    }*/

    @OnClick(R.id.btn_send_drivers)
    public void sendDrivers() {
        Intent intent = new Intent(this,
                AddDriverToTripActivity.class);
        // get number of drivers allow to company to add it
        int result = Integer.valueOf(trip.getNumberOfShipmentsAvailable()) - trip.getDriversCount();
        intent.putExtra("data", result);
        intent.putExtra("order_number", trip.getId());
        startActivityForResult(intent, 100);
    }

    @OnClick(R.id.tv_details)
    public void details() {
        Intent intent = new Intent(this, ViewTripForDriverStepTwoActivity.class);
        intent.putExtra("data", trip);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 100 && data != null) {
            int newDriverCount = data.getIntExtra("data", 0);
            if (newDriverCount > 0) {
                trip.setDriversCount(trip.getDriversCount() + newDriverCount);
                adapter.changeDriverCount(newDriverCount);

            }
        }
    }

}