package naaqll.com.naql.activities.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.DriverInTripAdapter;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.CarController;
import naaqll.com.naql.controller.DriverController;
import naaqll.com.naql.controller.DriversController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.Car;
import naaqll.com.naql.model.Driver;

public class AddDriverToTripActivity extends BaseActivity implements IAdapter, BaseController.IResult<Driver> {

   /* @BindView(R.id.et_driver_name)
    TextInputEditText etDriverName;
    @BindView(R.id.et_phone)
    TextInputEditText etPhone;*/
    @BindView(R.id.tv_allow)
    TextView tvAllow;
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.spinnerDriver)
    Spinner spinnerDriver;

    private DriverInTripAdapter adapter;
    private int driverCountAllowed = 0;
    private int tripId;
    private DriverController controller;
    private DriversController driversController;
    private CarController carController;
    private List<Car> cars;
    private List<Driver> drivers;
    private List<String> carName;
    private List<String> driverName;
    private ArrayAdapter<String> adapterDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_driver_to_trip);
        ButterKnife.bind(this);

        // get number of driver allowed to company
        driverCountAllowed = getIntent().getIntExtra("data", 0);
        tripId = getIntent().getIntExtra("order_number", 0);
        if (driverCountAllowed < 1) {
            Toast.makeText(this, getString(R.string.cant_add_drivers), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            initToolbar(R.id.toolbar);
            setTitle(getString(R.string.add_driver));
            adapter = new DriverInTripAdapter(this);
            recycler.setAdapter(adapter);
            tvAllow.append(" " + String.valueOf(driverCountAllowed));
            tvOrderNumber.append(" " + String.valueOf(tripId + Constants.TRIP_ID_START_FROM));
            controller = new DriverController(this, this);
            carController = new CarController(this, carIResult);
            driversController = new DriversController(this);
            carController.getCars();
        }

    }

    @OnClick(R.id.btn_add_driver)
    void onAddClick() {
        boolean check = true;
        if (cars == null) {
            controller.showMessage(getString(R.string.select_car));
            finish();
            check = false;
        }
        if (spinner.getSelectedItemPosition() < 0 || cars.get(spinner.getSelectedItemPosition()) == null) {
            controller.showMessage(getString(R.string.select_car));
            check = false;
        }
        if (spinnerDriver.getSelectedItemPosition() < 0 || drivers.get(spinnerDriver.getSelectedItemPosition()) == null) {
            controller.showMessage(getString(R.string.select_driver));
            check = false;
        }
        if (check) {
            adapter.addItem(new Driver(cars.get(spinner.getSelectedItemPosition()), drivers.get(spinnerDriver.getSelectedItemPosition()).getName(),
                    drivers.get(spinnerDriver.getSelectedItemPosition()).getPhone()));
            drivers.remove(spinnerDriver.getSelectedItemPosition());
            driverName.remove(spinnerDriver.getSelectedItemPosition());
            adapterDriver.notifyDataSetChanged();
            checkHaveDrivers();
        }
    }

    @OnClick(R.id.btn_save)
    public void save() {
        controller.addDriver(tripId, adapter.getList());
    }

    private boolean checkFiled(TextInputEditText input) {
        if (input.getText().toString().trim().isEmpty()) {
            input.setError(getString(R.string.required));
            return true;
        }
        return false;
    }

    @Override
    public void click(int position, Object o) {
        checkHaveDrivers();
    }

    private void checkHaveDrivers() {
        if (adapter.getItemCount() > 0) {
            btnSave.setVisibility(View.VISIBLE);
        } else {
            btnSave.setVisibility(View.GONE);
        }
    }

    @Override
    public void result(Driver driver) {
        // finish activity with driver size to add it in view trip details
        Intent intent = new Intent();
        intent.putExtra("data", adapter.getItemCount());
        setResult(RESULT_OK, intent);
        finish();
    }


    BaseController.IResult<List<Car>> carIResult = new BaseController.IResult<List<Car>>() {
        @Override
        public void result(List<Car> cars) {
            carName = new ArrayList<>();
            AddDriverToTripActivity.this.cars = cars;
            for (Car item : cars) {
                    carName.add(item.getCarType() + "(" + item.getPlateNumber() + ")");
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddDriverToTripActivity.this,
                    android.R.layout.simple_spinner_item, carName);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);

            // get driver
            driversController.getDriver(new BaseController.IResult<List<Driver>>() {
                @Override
                public void result(List<Driver> drivers) {
                    driverName = new ArrayList<>();
                    AddDriverToTripActivity.this.drivers = drivers;
                    for (Driver driver : drivers) {
                        driverName.add(driver.getName());
                    }
                    adapterDriver = new ArrayAdapter<String>(AddDriverToTripActivity.this,
                            android.R.layout.simple_spinner_item, driverName);
                    adapterDriver.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDriver.setAdapter(adapterDriver);
                }
            });
        }
    };
}
