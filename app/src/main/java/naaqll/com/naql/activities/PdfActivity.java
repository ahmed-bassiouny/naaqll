package naaqll.com.naql.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.net.http.SslError;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.time.format.TextStyle;
import java.util.HashMap;

import naaqll.com.naql.R;

public class PdfActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        // write the document content
        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }
        String targetPdf = directory_path+"test-2.pdf";
        File filePath = new File(targetPdf);


        WebView webView = findViewById(R.id.webView);
        /*
        //createPdf("hello");
        Document document = new Document();
        // Location to save
        try {
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            // Open to write
            document.open();
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("Android School");
            document.addCreator("Pratik Butani");

// Creating Chunk
            Chunk mOrderDetailsTitleChunk = new Chunk("Order Details");
// Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
// Setting Alignment for Heading
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_RIGHT);

// Finally Adding that Chunk
            document.add(mOrderDetailsTitleParagraph);

            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                    R.drawable.web);

            document.add(Image.getInstance(getBytesFromBitmap(icon)));

            PdfPTable table = createFirstTable();
            document.add(table);

            // step 5
            document.close();
            Toast.makeText(this, "DOne", Toast.LENGTH_SHORT).show();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        /*try {
            createPdf(filePath);
            Toast.makeText(this, "DOne", Toast.LENGTH_SHORT).show();

        } catch (IOException | DocumentException e) {
            e.printStackTrace();
            Log.e( "onCreate: ",e.getLocalizedMessage() );
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        }*/



        //prepare your html content which will be show in webview
       // String data = "<body>" + "<img src=\\"file:///android_asset/large_image.png\"/></body>"";

     /*   String htmlDocument = "<html><body>" +
                "<h1>Webview Print Test </h1>" +
                "<h2>I am Webview</h2>" +
                "<p> By Pointالسلام عليكم d</p>" +
                "<p> This is some sample content.</p>" +
                "<p> By PointOfAndroid</p>" +
                "<p> This is some sample content.</p>" +
                "<p> By PointOfAndroid</p>" +
                "<p> This is some sample content.</p>" +
                "" +
                "" +
                "" + "Put your content here" +
                "" +
                "" +
                "</body></html>";

        //load your html to webview
        webView.loadData(htmlDocument, "text/HTML", "UTF-8");*/

        webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        webView .loadUrl("http://www.google.com");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                createWebPrintJob(view);

            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            }

            @Override
            public void onReceivedHttpError(
                    WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                           SslError error) {
            }
        });

    }

    //create a function to create the print job
    private void createWebPrintJob(WebView webView) {

        //create object of print manager in your device
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

        //create object of print adapter
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        //provide name to your newly generated pdf file
        String jobName = getString(R.string.app_name) + " Print Test";

        //open print dialog
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }



    /**
     * Creates our first table
     * @return our first table
     */
    public static PdfPTable createFirstTable() {
        // a table with three columns
        PdfPTable table = new PdfPTable(3);
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("Cell with colspan 3"));
        cell.setColspan(3);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
        cell.setRowspan(2);
        table.addCell(cell);
        // we add the four remaining cells with addCell()
        table.addCell("row 1; cell 1");
        table.addCell("row 1; cell 2");
        table.addCell("row 2; cell 1");
        table.addCell("row 2; cell 2");
        return table;
    }
    public PdfPCell createImageCell(Bitmap bitmap) throws DocumentException, IOException {
        Image img = Image.getInstance(getBytesFromBitmap(bitmap));
        PdfPCell cell = new PdfPCell(img, true);

        return cell;
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public void createPdf(File dest) throws IOException, DocumentException {
        Document document = new Document();


        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new int[]{1, 6});
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.web);
        table.addCell(createImageCell(icon));
        table.addCell(createTextCell("this iنمكبنابل title"));
        table.addCell(creatEmptyCell());
        table.addCell(creatEmptyCell());

        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("Cell with colspan 3"));
        cell.setColspan(3);

        cell.setVerticalAlignment(Element.ALIGN_CENTER);

        table.addCell(cell);
        // now we add a cell with rowspan 2
        cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
        cell.setRowspan(2);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);

        table.addCell(cell);
        // we add the four remaining cells with addCell()
        table.addCell("row 1; cell 1");
        table.addCell("row 1; cell 2");
        table.addCell("row 2; cell 1");
        table.addCell("row 2; cell 2");


        document.add(table);



        /*PdfPCell cell = new PdfPCell();
        cell.addElement(createFirstTable());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingTop(100);


        PdfPTable table2 = new PdfPTable(1);
        table2.setWidthPercentage(100);*/

        //PdfPTable table2
        //document.add(createFirstTable());

        document.close();
    }

    public  PdfPCell createTextCell(String text) throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        Paragraph p = new Paragraph(text);
        p.setAlignment(Element.ALIGN_RIGHT);




       // BaseFont urName = BaseFont.createFont(, "UTF-8",BaseFont.EMBEDDED);

        Font urFontName = new Font(Font.FontFamily.HELVETICA, 12);
        p.setFont(urFontName);
        cell.addElement(p);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    public static PdfPCell creatEmptyCell() throws DocumentException, IOException {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(50);
        return cell;
    }


}
