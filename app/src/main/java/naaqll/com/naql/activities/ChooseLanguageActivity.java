package naaqll.com.naql.activities;

import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.controller.AuthController;

public class ChooseLanguageActivity extends BaseActivity {

    private AuthController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        ButterKnife.bind(this);
        controller = new AuthController(this);
    }

    @OnClick(R.id.linear_english)
    public void setEnglishLang() {
        controller.selectEnglish();
    }

    @OnClick(R.id.linear_arabic)
    public void setArabicLang() {
        controller.selectArabic();
    }


}
