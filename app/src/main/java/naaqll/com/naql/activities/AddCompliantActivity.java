package naaqll.com.naql.activities;

import android.os.Bundle;
import android.widget.EditText;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.controller.AddCompliantController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.User;

public class AddCompliantActivity extends BaseActivity {


    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.et_compliant)
    EditText etCompliant;

    private AddCompliantController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_compliant);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.create_compliant));
        setDataUI();
        controller = new AddCompliantController(this);
    }

    private void setDataUI() {
        User user = SharedPrefManager.getObject(Constants.USER, User.class);
        etPhone.setText(user.getPhone());
        etEmail.setText(user.getEmail());
    }

    @OnClick(R.id.btn_send)
    public void create() {
        if (etPhone.getText().toString().trim().isEmpty()) {
            etPhone.setError(getString(R.string.required));
        } else if (etEmail.getText().toString().trim().isEmpty()) {
            etEmail.setError(getString(R.string.required));
        } else if (etTitle.getText().toString().trim().isEmpty()) {
            etTitle.setError(getString(R.string.required));
        } else if (etCompliant.getText().toString().trim().isEmpty()) {
            etCompliant.setError(getString(R.string.required));
        } else {
            controller.sendCompliant(etEmail.getText().toString(),
                    etPhone.getText().toString(), etTitle.getText().toString(),
                    etCompliant.getText().toString());
        }
    }
}
