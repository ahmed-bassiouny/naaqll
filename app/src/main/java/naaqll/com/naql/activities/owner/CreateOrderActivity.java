package naaqll.com.naql.activities.owner;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.Calendar;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.CreateOrderController;
import naaqll.com.naql.custom_views.CustomButton;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.Utils;
import naaqll.com.naql.model.CitiesAndRegions;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;

public class CreateOrderActivity extends BaseActivity {


    @BindView(R.id.et_company_name)
    EditText etCompanyName;
    @BindView(R.id.ch_request_hint)
    CheckBox chRequestHint;
    @BindView(R.id.ch_permission_hint)
    CheckBox chPermissionHint;
    @BindView(R.id.sp_products_type)
    Spinner spProductType;
    @BindView(R.id.sp_trip_type)
    Spinner spTripType;
    @BindView(R.id.et_shipments)
    EditText etShipments;
    @BindView(R.id.et_load)
    EditText etLoad;
    @BindView(R.id.et_price)
    EditText etPrice;
    @BindView(R.id.btn_create)
    CustomButton btnCreate;

    @BindView(R.id.rb_from_ksa)
    RadioButton rbFromKsa;
    @BindView(R.id.from_city)
    TextView fromCity;
    @BindView(R.id.from_region)
    TextView fromRegion;

    @BindView(R.id.rb_to_ksa)
    RadioButton rbToKsa;
    @BindView(R.id.to_city)
    TextView toCity;
    @BindView(R.id.to_region)
    TextView toRegion;


    @BindView(R.id.et_start_location)
    EditText etStartLocation;
    @BindView(R.id.et_end_location)
    EditText etEndLocation;
    @BindView(R.id.sp_from_time)
    Spinner spFromTime;
    @BindView(R.id.sp_to_time)
    Spinner spToTime;
    @BindView(R.id.et_holiday)
    EditText etHoliday;


    @BindView(R.id.rg_return_group)
    RadioGroup rgReturnGroup;
    @BindView(R.id.linear_return_region)
    LinearLayout linearReturnRegion;
    @BindView(R.id.linear_return_city)
    LinearLayout linearReturnCity;

    @BindView(R.id.rb_return_ksa)
    RadioButton rbReturnKsa;
    @BindView(R.id.return_region)
    TextView returnRegion;
    @BindView(R.id.return_city)
    TextView returnCity;

    @BindView(R.id.et_return_location)
    EditText etReturnLocation;
    @BindView(R.id.sp_saving_cost)
    Spinner spSavingCost;

    @BindView(R.id.et_type_of_load)
    EditText etTypeOfLoad;
    @BindView(R.id.et_desc_of_load)
    EditText etDescOfLoad;
    @BindView(R.id.sp_payment_method)
    Spinner spPaymentMethod;

    private CreateOrderController controller;
    private double fromLat, fromLng, toLat, toLng, returnLat, returnLng;

    private String[] arrs;

    private int fromCityIndex = -1;
    private int fromRegionIndex = -1;
    private int toCityIndex = -1;
    private int toRegionIndex = -1;
    private int returnCityIndex = -1;
    private int returnRegionIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.create_request));
        ButterKnife.bind(this);
        controller = new CreateOrderController(this);
        etCompanyName.setText(SharedPrefManager.getObject(Constants.USER, User.class).getCompanyName());
        spTripType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    linearReturnRegion.setVisibility(View.GONE);
                    linearReturnCity.setVisibility(View.GONE);
                    etReturnLocation.setVisibility(View.GONE);
                    rgReturnGroup.setVisibility(View.GONE);
                } else {
                    linearReturnRegion.setVisibility(View.VISIBLE);
                    linearReturnCity.setVisibility(View.VISIBLE);
                    etReturnLocation.setVisibility(View.VISIBLE);
                    rgReturnGroup.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getSelectedCachTime();
/*

        etLoad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    int count = Integer.parseInt(etLoad.getText().toString());
                    if (count >= 30) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateOrderActivity.this);
                        builder1.setTitle(getString(R.string.overload));
                        builder1.setMessage(getString(R.string.permit));
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                getString(R.string.yes),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                } catch (Exception e) {
                }
            }
        });
*/


    }


    public void getSelectedCachTime() {
        int fromTimeIndex = SharedPrefManager.getInteger(Constants.FROM_TIME);
        int toTimeIndex = SharedPrefManager.getInteger(Constants.TO_TIME);

        spFromTime.setSelection(fromTimeIndex);
        spToTime.setSelection(toTimeIndex);
    }

    @OnClick(R.id.from_city)
    public void fromCityClick() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbFromKsa.isChecked()) {
            arrs = CitiesAndRegions.getCitiesKSA();
        } else {
            arrs = CitiesAndRegions.getCitiesAUE();
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fromCity.setText(arrs[which]);
                fromCityIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.from_region)
    public void fromRegionClick() {
        if (fromCityIndex == -1) {
            controller.showErrorMessage(getString(R.string.please_choose_region));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbFromKsa.isChecked()) {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.ksaIndex, fromCityIndex);
        } else {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.aueIndex, fromCityIndex);
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fromRegion.setText(arrs[which]);
                fromRegionIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @OnClick(R.id.to_city)
    public void toCityClick() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbToKsa.isChecked()) {
            arrs = CitiesAndRegions.getCitiesKSA();
        } else {
            arrs = CitiesAndRegions.getCitiesAUE();
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                toCity.setText(arrs[which]);
                toCityIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.to_region)
    public void toRegionClick() {
        if (toCityIndex == -1) {
            controller.showErrorMessage(getString(R.string.please_choose_region));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbToKsa.isChecked()) {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.ksaIndex, toCityIndex);
        } else {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.aueIndex, toCityIndex);
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                toRegion.setText(arrs[which]);
                toRegionIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @OnClick(R.id.return_city)
    public void returnCityClick() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbReturnKsa.isChecked()) {
            arrs = CitiesAndRegions.getCitiesKSA();
        } else {
            arrs = CitiesAndRegions.getCitiesAUE();
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                returnCity.setText(arrs[which]);
                returnCityIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.return_region)
    public void returnRegionClick() {
        if (returnCityIndex == -1) {
            controller.showErrorMessage(getString(R.string.please_choose_region));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateOrderActivity.this);
        if (rbReturnKsa.isChecked()) {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.ksaIndex, returnCityIndex);
        } else {
            arrs = CitiesAndRegions.getRegion(CitiesAndRegions.aueIndex, returnCityIndex);
        }

        builder.setItems(arrs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                returnRegion.setText(arrs[which]);
                returnRegionIndex = which;
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @OnClick(R.id.btn_create)
    public void create() {
        if (validData()) {
            saveTrip();
        }
    }

    private void saveTrip() {

        if (fromCityIndex == -1 || fromRegionIndex == -1 || toCityIndex == -1 || toRegionIndex == -1) {
            controller.showErrorMessage(getString(R.string.required_fileds));
            return;
        }

        // check if load  not greater than 30 or 150 in (loped case index 3 in array)
        int load = Integer.parseInt(etLoad.getText().toString());
        if ( load > 150 || (spProductType.getSelectedItemPosition() != 3 &&  load > 30)) {
            controller.showErrorMessage(getString(R.string.invalid_load_size));
            return;
        }

        if (Integer.parseInt(etPrice.getText().toString()) > 500000){
            controller.showErrorMessage(getString(R.string.invalid_price));
            return;
        }
        Integer fromCityId, fromId, toId, returnId;


        if (rbFromKsa.isChecked()) {
            fromCityId = CitiesAndRegions.getCityId(CitiesAndRegions.ksaIndex, fromCityIndex);
        } else {
            fromCityId = CitiesAndRegions.getCityId(CitiesAndRegions.aueIndex, fromCityIndex);
        }


        if (rbFromKsa.isChecked()) {
            fromId = CitiesAndRegions.getRegionId(CitiesAndRegions.ksaIndex, fromCityIndex, fromRegionIndex);
        } else {
            fromId = CitiesAndRegions.getRegionId(CitiesAndRegions.aueIndex, fromCityIndex, fromRegionIndex);
        }

        if (rbToKsa.isChecked()) {
            toId = CitiesAndRegions.getRegionId(CitiesAndRegions.ksaIndex, toCityIndex, toRegionIndex);
        } else {
            toId = CitiesAndRegions.getRegionId(CitiesAndRegions.aueIndex, toCityIndex, toRegionIndex);
        }

        String fromTime, toTime;


        SharedPrefManager.setInteger(Constants.FROM_TIME, spFromTime.getSelectedItemPosition());
        SharedPrefManager.setInteger(Constants.TO_TIME, spToTime.getSelectedItemPosition());
        fromTime = spFromTime.getSelectedItem().toString();
        toTime = spToTime.getSelectedItem().toString();


        // trip type => one way or round trip
        // i save it by index

        // saving cost => 4 type
        // i save it by index

        Trip trip = new Trip(
                getString(R.string.ksa), String.valueOf(fromId), etStartLocation.getText().toString(), fromLat, fromLng,
                String.valueOf(toId), etEndLocation.getText().toString(), toLat, toLng,
                etCompanyName.getText().toString(), String.valueOf(spProductType.getSelectedItemPosition()),
                spTripType.getSelectedItemPosition(), etShipments.getText().toString(), etLoad.getText().toString(),
                etPrice.getText().toString(), fromTime, toTime,
                etHoliday.getText().toString(), fromCityId, spSavingCost.getSelectedItemPosition(),
                chRequestHint.isChecked(), etTypeOfLoad.getText().toString(),
                etDescOfLoad.getText().toString(), spPaymentMethod.getSelectedItemPosition()
        );
        if (spTripType.getSelectedItemPosition() != 0) {
            if (returnCityIndex == -1 || returnRegionIndex == -1) {
                controller.showErrorMessage(getString(R.string.required_fileds));
                return;
            }
            if (rbReturnKsa.isChecked()) {
                returnId = CitiesAndRegions.getRegionId(CitiesAndRegions.ksaIndex, returnCityIndex, returnRegionIndex);
            } else {
                returnId = CitiesAndRegions.getRegionId(CitiesAndRegions.aueIndex, returnCityIndex, returnRegionIndex);
            }
            trip.setReturnLat(returnLat);
            trip.setReturnLng(returnLng);
            trip.setReturnAddress(etReturnLocation.getText().toString());
            trip.setReturnCity(String.valueOf(returnId));
        }
        controller.createTrip(trip, new BaseController.IResult<Trip>() {
            @Override
            public void result(Trip trip) {
                Intent data = new Intent();
                data.putExtra("data", trip);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private void selectTime(final EditText item) {
        item.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateOrderActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        item.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("اختر مواعيد العمل");
                mTimePicker.show();

            }
        });
    }

    private boolean validData() {
        // check error
        if (Utils.validEditText(etCompanyName) &&
                Utils.validEditText(etShipments) &&
                Utils.validEditText(etLoad) && Utils.validEditText(etPrice)) {
            return true;
        }
        return false;
    }

    @OnClick(R.id.et_start_location)
    public void startLocation() {
        etStartLocation.setEnabled(false);
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), 100);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.et_end_location)
    public void endLocation() {
        etEndLocation.setEnabled(false);
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), 110);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.et_return_location)
    public void returnLocation() {
        etEndLocation.setEnabled(false);
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), 120);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            etStartLocation.setEnabled(true);
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                fromLat = place.getLatLng().latitude;
                fromLng = place.getLatLng().longitude;
                if (place.getAddress() != null && place.getAddress().toString().isEmpty()) {
                    etStartLocation.setText(place.getLatLng().latitude + "," + place.getLatLng().longitude);
                } else if (place.getAddress() != null) {
                    etStartLocation.setText(place.getAddress());

                }
            }
        } else if (requestCode == 110) {
            etEndLocation.setEnabled(true);
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                toLat = place.getLatLng().latitude;
                toLng = place.getLatLng().longitude;
                if (place.getAddress() != null && place.getAddress().toString().isEmpty()) {
                    etEndLocation.setText(place.getLatLng().latitude + "," + place.getLatLng().longitude);
                } else if (place.getAddress() != null) {
                    etEndLocation.setText(place.getAddress());

                }
            }
        } else if (requestCode == 120) {
            etEndLocation.setEnabled(true);
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                returnLat = place.getLatLng().latitude;
                returnLng = place.getLatLng().longitude;
                if (place.getAddress() != null && place.getAddress().toString().isEmpty()) {
                    etReturnLocation.setText(place.getLatLng().latitude + "," + place.getLatLng().longitude);
                } else if (place.getAddress() != null) {
                    etReturnLocation.setText(place.getAddress());

                }
            }
        }
    }
}