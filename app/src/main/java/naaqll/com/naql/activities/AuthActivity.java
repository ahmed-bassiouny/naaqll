package naaqll.com.naql.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.fragments.AboutUsFragment;
import naaqll.com.naql.fragments.LoginFragment;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.WebPageType;


public class AuthActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        MyFragmentTransaction.open(this, new LoginFragment(),R.id.main_frame,null);
    }
}
