package naaqll.com.naql.activities.owner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.PackageAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.PackageController;
import naaqll.com.naql.model.PackageTrip;

public class PackageActivity extends BaseActivity implements BaseController.IResult<List<PackageTrip>> {

    @BindView(R.id.recycler)
    RecyclerView recycler;

    private PackageController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.packages));
        controller = new PackageController(this);
        controller.getPackage(this);
    }

    @Override
    public void result(List<PackageTrip> packageTrips) {
        recycler.setAdapter(new PackageAdapter(packageTrips));
    }
}
