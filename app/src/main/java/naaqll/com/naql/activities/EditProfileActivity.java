package naaqll.com.naql.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import bassiouny.ahmed.genericmanager.MyFragmentTransaction;
import naaqll.com.naql.R;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.fragments.EditProfileFragment;

public class EditProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.edit_profile));
        MyFragmentTransaction.open(this, new EditProfileFragment(),R.id.main_frame,null);
    }
}
