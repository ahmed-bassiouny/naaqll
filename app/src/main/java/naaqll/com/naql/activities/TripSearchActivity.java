package naaqll.com.naql.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import bassiouny.ahmed.genericmanager.SharedPrefManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.adapter.IAdapter;
import naaqll.com.naql.adapter.TripForOwnerAdapter;
import naaqll.com.naql.base.ui.BaseActivity;
import naaqll.com.naql.base.ui.BaseController;
import naaqll.com.naql.controller.SearchController;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.model.Trip;
import naaqll.com.naql.model.User;
import naaqll.com.naql.model.UserType;

public class TripSearchActivity extends BaseActivity implements IAdapter<Trip>,BaseController.IResult<List<Trip>> {

    @BindView(R.id.et_bill_number)
    EditText etBillNumber;
    @BindView(R.id.et_date_from)
    EditText etDateFrom;
    @BindView(R.id.et_date_to)
    EditText etDateTo;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    private TripForOwnerAdapter adapter;
    private SearchController controller;
    private UserType userType;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_search);
        ButterKnife.bind(this);
        initToolbar(R.id.toolbar);
        setTitle(getString(R.string.search_for_bill));
        controller = new SearchController(this);
        adapter = new TripForOwnerAdapter(this, this);
        recycler.setAdapter(adapter);
        User user = SharedPrefManager.getObject(Constants.USER,User.class);
        userId = user.getId();
        userType = user.getUserType();
    }

    private void checkInputs(){
        if(etBillNumber.getText().toString().trim().isEmpty() &&
                etDateFrom.getText().toString().trim().isEmpty() &&
                etDateTo.getText().toString().trim().isEmpty()){
            // not valid
            controller.showErrorMessage(getString(R.string.required_fileds));
        }else if (etBillNumber.getText().toString().trim().isEmpty()){
            if(etDateFrom.getText().toString().trim().isEmpty() ||
                    etDateTo.getText().toString().trim().isEmpty()){
                // not valid
                controller.showErrorMessage(getString(R.string.required_fileds));
            }else {
                // valid
                controller.search(etBillNumber.getText().toString(),
                        etDateFrom.getText().toString(),
                        etDateTo.getText().toString(),this);
            }
        }else {
            if (userType == UserType.DRIVER_COMPANIES) {
                String billNumber = etBillNumber.getText().toString();
                if (billNumber.startsWith(""+userId)){
                    int length = (userId+"").length();
                    billNumber = billNumber.substring(length);
                    if(billNumber.isEmpty()){
                        controller.showErrorMessage(getString(R.string.invalid_bill_number));
                        return;
                    }
                    controller.search(billNumber,
                            etDateFrom.getText().toString(),
                            etDateTo.getText().toString(), this);
                }else {
                    controller.showErrorMessage(getString(R.string.invalid_bill_number));
                }
            }else {
                controller.search(etBillNumber.getText().toString(),
                        etDateFrom.getText().toString(),
                        etDateTo.getText().toString(), this);
            }
        }
    }

    @OnClick(R.id.btn_search)
    void search(){
        if(adapter != null)
        adapter.setList(new ArrayList<Trip>());
        checkInputs();
    }

    @Override
    public void click(int position, Trip trip) {
        Intent intent = new Intent(this,TripWebViewActivity.class);
        if (userType == UserType.DRIVER_COMPANIES){
            intent.putExtra("data","http://naaqll.com/naaqll.com/public/orders/drivers/"+trip.getId()+"/"+userId);
        }else {
            intent.putExtra("data","http://naaqll.com/naaqll.com/public/orders/company/drivers/"+trip.getId());
        }
        startActivity(intent);
    }

    @OnClick(R.id.et_date_from)
    public void selectFromDate() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        DatePickerDialog dialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        etDateFrom.setText(String.format(Locale.getDefault(), "%d-%d-%d", year, (1 + monthOfYear), dayOfMonth));

                    }
                }, mYear, mMonth, mDay);
        dialog.getDatePicker().setMaxDate(c.getTime().getTime());
        dialog.show();
    }

    @OnClick(R.id.et_date_to)
    public void selectToDate() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        DatePickerDialog dialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        etDateTo.setText(String.format(Locale.getDefault(), "%d-%d-%d", year, (1 + monthOfYear), dayOfMonth));

                    }
                }, mYear, mMonth, mDay);
        dialog.getDatePicker().setMaxDate(c.getTime().getTime());
        dialog.show();
    }

    @Override
    public void result(List<Trip> trips) {
        if(trips.size() == 0){
            controller.showMessage(getString(R.string.no_trips_found));
        }else {
            adapter.setList(trips);
        }
    }
}
