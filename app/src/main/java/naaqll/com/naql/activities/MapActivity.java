package naaqll.com.naql.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;
import naaqll.com.naql.R;
import naaqll.com.naql.helper.Constants;
import naaqll.com.naql.helper.LocationManager;
import naaqll.com.naql.model.MyPlace;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private final int MY_PERMISSION_REQUEST_CODE = 56;
    private GoogleMap mobileMap;
    private LocationManager locationManager;
    private Location myLocation;
    private boolean showDefaultLocation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSION_REQUEST_CODE);
        } else {
            locationManager = new LocationManager(this, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mobileMap = googleMap;
        if(showDefaultLocation) {
            setLocationOnMap(24.713552, 46.675297);
        }

    }

    private void setLocationOnMap(double lat, double lng) {
        LatLng myLocation = new LatLng(lat, lng);
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(myLocation);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);

        mobileMap.moveCamera(center);
        mobileMap.moveCamera(zoom);

        mobileMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
    }


    @OnClick(R.id.picker_btn)
    public void selectLocation() {
        LatLng pickedLoc = mobileMap.getCameraPosition().target;
        Locale loc = new Locale("ar");
        Geocoder geocoder = new Geocoder(this, loc);
        try {
            if(geocoder.getFromLocation(pickedLoc.latitude, pickedLoc.longitude, 1).size()>0) {
                MyPlace place = new MyPlace(pickedLoc.latitude, pickedLoc.longitude
                        , geocoder.getFromLocation(pickedLoc.latitude, pickedLoc.longitude, 1).get(0));
                Intent data = new Intent();
                data.putExtra(Constants.LOCATION, place);
                setResult(RESULT_OK, data);
                finish();
            }else {
                Toast.makeText(this, getString(R.string.place_not_correct), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
        showDefaultLocation = false;
        setLocationOnMap(location.getLatitude(), location.getLongitude());
        locationManager.removeListener(this);
    }

    @OnClick(R.id.map_my_location)
    public void findLocation() {
        if (myLocation != null)
            setLocationOnMap(myLocation.getLatitude(), myLocation.getLongitude());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                locationManager = new LocationManager(this, this);
        }
    }

/*    @OnClick(R.id.search)
    public void search() {
        Intent intent;
        try {
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                setLocationOnMap(place.getLatLng().latitude, place.getLatLng().longitude);
            } else {
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            }
        }
    }*/
}
